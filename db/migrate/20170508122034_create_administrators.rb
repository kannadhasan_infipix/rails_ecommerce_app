class CreateAdministrators < ActiveRecord::Migration
  def change
    create_table :administrators do |t|
      t.string :profile_name
      t.string :email
      t.string :salt
      t.string :password
      t.string :auth_token
      t.integer :status

      t.timestamps null: false
    end
  end
end
