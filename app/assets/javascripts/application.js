// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require turbolinks
// //= require_tree .


// $(document).ready(function(){
//   var userOnline = "<%= cookies[:user_auth_token] %>";
//   console.log("userOnline check",userOnline);
//   $(".profile_tag").hide();
//   $(".topbar_login").hide();
//   if(!userOnline){
//     $(".topbar_login").show();
//   }else{
//       $(".profile_tag").show();
//   }
//   $(".item_add11").click(function(e) {
//    var user_auth_token = "<%= cookies[:user_auth_token] %>";
//    console.log("user_auth_token", user_auth_token);
//    if(!user_auth_token){
//     $("#user_login_modal_btn").trigger("click");
//     return false;
//    }

//     var item_id = $(this).attr("itemid");
//     var shopping_cart = {"shopping_cart":{"item_id":item_id,"status":"active"}}
//     console.log("add to cart data",shopping_cart);
//     $.ajax({
//       type: "POST",
//       beforeSend: function(request) {
//         request.setRequestHeader("Authorization", "Token token="+user_auth_token);
//       },
//       url: "/api/shopping_cart/v1/shopping_cart/create",
//       data: shopping_cart,
//       cache: false,
//       dataType: "json",
//       success: function(response){
//         console.log("item added to shopping_cart");
//         if(response.status){
//           $(".user_cart_amount").text(response.useraccount.cart_amount);
//           $(".user_cart_count").text(response.useraccount.cart_count);
//           $("#view_cart").attr("href","/api/shopping_cart/v1/shopping_cart/");
//           alert("Item added to cart ");
//           console.log("cart_items",response.data.cart_items);
          
//         }else{
//           alert(response.errors)
//         }
//       },
//       error: function(xhr, status, error) {
//         alert(xhr.status+"  "+error)
//       }
//     });
//     e.preventDefault();
//   });




// });

