class Api::Payment::V1::PaymentController < Api::Payment::V1::ApiController
  skip_before_filter :verify_authenticity_token
  before_action :display_menus, :user_account_detail, :authenticate
  def new
    @payment = ::Payment.new
  end

  def index
    @payment = ::Payment.all
  end

  def show
    begin
      @payment = ::Payment.where("order_id=?",params[:order_id])
      render json: {status: true, payment: @payment}
    rescue ActiveRecord::RecordNotFound => e
      @payment = []
    end
  end

  def edit
    begin
      @payment = ::Payment.find(params[:id])
    rescue ActiveRecord::RecordNotFound => e
      @payment = []
    end
  end

  def create
    begin
      payment_exist = ::Payment.find_by("order_id=? ",  params[:order_id]) rescue nil 
      render json: {status: false, errors: ['Payment already exist.']} and return if !payment_exist.blank?
      @payment = ::Payment.new(create_params)
      if @payment.save!
        cookies[:payment_session_id] =  @payment.session_id
        redirect_to "/api/order/v1/order"
        #render json: {status: true, data: {payment: @payment}}
      else
        errors = @payment.errors.messages.each.map {|k,v| v}
        render json: {status: false, errors: errors.flatten }
      end
    rescue Exception => e  
      render json: {status: :false, error: "Payment creation failed" ,  error_message:e.message} 
    end
  end

  def update
    begin
      @payment = ::Payment.find(params[:id])
      if @payment.update(create_params)
        render json: {status: true, data: {payment: @payment}}
      else
        errors = @payment.errors.messages.each.map {|k,v| v}
        render json: {status: false, errors: errors.flatten }
      end
    rescue ActiveRecord::RecordNotFound => e
      render json: {status: :false, error: "Payment not found",  error_message:e.message} 
    end
  end

  def delete
    begin
      @payment = ::Payment.find(params[:id])
      if @payment.destroy
        render json: {status: true, notice: 'Payment is successfully destroyed.'}
      else
        render json: {status: :false, error: "Payment details deleted failed."}
      end 
    rescue ActiveRecord::RecordNotFound => e
      render json: {status: false, error: "Payment not found",  error_message:e.message} 
    end
  end

  def update_status
    begin
      @payment = ::Payment.find_by("session_id=?",params[:session_id])
      @payment.status = params[:status]
      if @payment.save!
        if @payment.status == "Success"  
          #UserPaymentMailer.send_payment_information(@current_user.email).deliver_later
        end
        @payment
        # render json: {status: true, data:@payment}
      else
        errors = @payment.errors.messages.each.map {|k,v| v}
        render json: {status: false, errors: errors.flatten }
      end
    rescue ActiveRecord::RecordNotFound => e
      render json: {status: :false, error: "Payment not found",  error_message:e.message} 
    end
  end
 
  private

  def create_params
    params.permit( :transaction_id, :pgi_id, :order_id, :amount, :status)
  end
end
