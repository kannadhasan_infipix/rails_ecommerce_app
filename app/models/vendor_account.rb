class VendorAccount < ActiveRecord::Base
  belongs_to :vendor
  enum status: {draft: 0, active: 1, deactive: 2}
end
