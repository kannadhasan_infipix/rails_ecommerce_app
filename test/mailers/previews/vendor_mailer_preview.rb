# Preview all emails at http://localhost:3000/rails/mailers/vendor_mailer
class VendorMailerPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/vendor_mailer/send_email_verification_token
  def send_email_verification_token
    VendorMailer.send_email_verification_token
  end

end
