class Api::Vendor::V1::VendorController < Api::Vendor::V1::ApiController
  skip_before_filter :verify_authenticity_token
  before_action :display_menus, :user_account_detail
  before_action :authenticate, :except => [:index, :login]
  def index
    #@vendor = ::Vendor.all
  end

  def show
    begin
      @order = ::Item.select("orders.id as o_id, orders.order_id, orders.order_quantity, orders.order_size, orders.order_color, orders.date_of_purchase, orders.payable_amount,orders.status, items.id ,items.name,items.image,item_inventories.price,items.created_at").joins(:item_inventory).joins(:Order).where("orders.vendor_id=?",@current_user.id).uniq.order("items.created_at DESC")
      @myproductlist = ::Item.select(" items.id  ,items.name, items.image,items.created_at , item_inventories.*").joins(:item_inventory).where("items.published_by=?",@current_user.id).uniq.order("items.created_at DESC")
      #render json: {status: @order}
    rescue ActiveRecord::RecordNotFound => e
      @vendor = []
    end
  end

  def edit_vendor_product
    @edit_my_item = ::Item.select("items.*,stores.id as store_id ,stores.name as store_name,item_inventories.id as iv_id,item_inventories.price,item_inventories.mrp,item_inventories.discount,item_inventories.instock,brands.id as brand_id,brands.name as brand_name, menus.id as menu_id,menus.name as menu_name, categories.id as cat_id,categories.name as cate_name").joins(item_inventory: :store).joins(:brand).joins(:menu).joins(:category).where("items.id=?", params[:id]) 
    #render json: {status: true, data: @edit_my_item}
  end

  def new
    @vendor = ::Vendor.new
  end

  def edit
    begin
      @vendor = ::Vendor.find(params[:id])
    rescue ActiveRecord::RecordNotFound => e
      @vendor = []
    end
  end

  def vendor_order
     begin
      @customer_details = ::User.select("users.firstname, users.lastname, users.email, users.mobile_number").joins(:order).where("orders.id=?",params[:id])
      @item_details = ::Item.select("items.name, items.image,item_inventories.price, item_inventories.mrp, item_inventories.discount").joins(:item_inventory).joins(:Order).where("orders.id=? and orders.item_id=item_inventories.item_id and item_inventories.vendor_id=?",params[:id], @current_user.id)
      @order_details = ::Order.find(params[:id])
      render json: {status: true, data: {customer: @customer_details, item: @item_details, order: @order_details }}
    rescue ActiveRecord::RecordNotFound => e
      @order = []
       render json: {status: :false, error: "Order not found" ,  error_message:e.message} 
    end
  end

  def create
    begin
      vendor_exist = ::Vendor.find_by_email(params[:vendor][:email]) rescue nil
      render json: {status: false, errors: ['Email already registered.']} and return if !vendor_exist.blank?
      @vendor = ::Vendor.new(create_params)
      if @vendor.save!
        #VendorMailer.send_email_verification_token(@vendor).deliver_later
       render json: {status: true, vendor_auth_token: @vendor.auth_token}
      else
        errors = @vendor.errors.messages.each.map {|k,v| v}
        render json: {status: false, errors: errors.flatten }
      end
    rescue Exception => e  
      render json: {status: :false, error: "Vendor creation failed" ,  error_message:e.message} 
    end
  end

  def update
     begin
      @vendor = ::Vendor.find(params[:id])
      if @vendor.update(create_params)
        render json: {status: true, data: {vendor: @vendor}}
      else
        errors = @vendor.errors.messages.each.map {|k,v| v}
        render json: {status: false, errors: errors.flatten }
      end
    rescue ActiveRecord::RecordNotFound => e
      render json: {status: :false, error: "Vendor not found",  error_message:e.message} 
    end
  end

  def delete
     begin
      @vendor = ::Vendor.find(params[:id])
      if @vendor.destroy
        render json: {status: true, notice: 'Vendor is successfully destroyed.'}
      else
        render json: {status: :false, error: "Vendor details deleted failed."}
      end 
    rescue ActiveRecord::RecordNotFound => e
      render json: {status: false, error: "Vendor not found",  error_message:e.message} 
    end
  end
  
  def login
    render json: {status: false, errors: ['Email or Password is empty.']} and return if login_details_blank?
    vendor = ::Vendor.find_by_email(params[:email]) rescue nil

    unless vendor
      render json: {status: false, errors: ['No account exists with this email.']} and return
    end
    if authenticate_login(vendor)
      cookies[:vendor_auth_token] =  vendor.auth_token
      render json: {status: true, vendor_auth_token: vendor.auth_token}
    else
      render json: {status: false, errors: ['Email and Password does not match.']} and return
    end
  end

  def logout
    cookies.delete(:vendor_auth_token)
    redirect_to root_path
  end

  def forgot_password
    begin
      @vendor = ::Vendor.find_by_email(params[:email]) 
      encrypted_password = PasswordEncryptor.new(params[:password]).salted
      @vendor.salt = encrypted_password.salt
      @vendor.password = encrypted_password.hash
      
      if @vendor.save!
        flash[:notice] = "Vendor Password successfully Updated"
        #redirect_to api_vendor_v1_vendor_path
      else
        errors = @vendor.errors.messages.each.map {|k,v| v}
        render json: {status: false, errors: errors.flatten }
      end
    rescue ActiveRecord::RecordNotFound => e
      render json: {status: :false, error: "Vendor not found",  error_message:e.message} 
    end
  end
  def update_image
    begin
      @vendor = ::Vendor.find(params[:id])
      @vendor.status = params[:status]
      if @vendor.save!
        flash[:notice] = "Vendor status successfully updated"
      else
        errors = @vendor.errors.messages.each.map {|k,v| v}
        render json: {status: false, errors: errors.flatten }
      end
    rescue ActiveRecord::RecordNotFound => e
      render json: {status: :false, error: "Vendor not found",  error_message:e.message} 
    end
  end

  def update_status
    begin
      @vendor = ::Vendor.find(params[:id])
      @vendor.image = params[:image]
      
      if @vendor.save!
        flash[:notice] = "Vendor image successfully Updated"
      else
        errors = @vendor.errors.messages.each.map {|k,v| v}
        render json: {status: false, errors: errors.flatten }
      end
    rescue ActiveRecord::RecordNotFound => e
      render json: {status: :false, error: "Vendor not found",  error_message:e.message} 
    end
  end

  def add_product
    begin
      item_exist = ::Item.find_by_name(params[:item][:name]) rescue nil
      render json: {status: false, errors: ['Item already exist.']} and return if !item_exist.blank?
      @item = ::Item.new(add_product_params_with_vendor)
      @item.features = params[:item][:features]
      @item.item_inventory.build(:vendor_id => @current_user.id, :store_id => params[:item_inventory][:store_id] , :price => params[:item_inventory][:price] , :mrp =>params[:item_inventory][:mrp]  , :instock => params[:item_inventory][:instock] , :status => 'active')
      if @item.save!
        render json: {status: true, data: {item: @item}}
      else
        errors = @item.errors.messages.each.map {|k,v| v}
        render json: {status: @item, errors: errors.flatten }
      end
    rescue Exception => e  
      render json: {status: false, error: "Item creation failed" ,  error_message:e.message} 
    end
  end

  def add_inventory
    begin
     item_inventory_exist = ::ItemInventory.find_by("item_id=? and vendor_id=? and store_id=?", params[:item_inventory][:item_id],@current_user.id,params[:item_inventory][:store_id]) rescue nil
     render json: {status: false, errors: ['Item Inventory already exist.']} and return if !item_inventory_exist.blank?
     @item_inventory = ::ItemInventory.new(inventroy_params_with_user)
      if @item_inventory.save!
        render json: {status: true, data: {item_inventory: @item_inventory}}
      else
        errors = @item_inventory.errors.messages.each.map {|k,v| v}
        render json: {status: false, errors: errors.flatten }
      end
    rescue Exception => e  
      render json: {status: :false, error: "Item Inventory creation failed" ,  error_message:e.message} 
    end
    
  end

  def update_vendor_product
    begin
      item_exist = ::Item.find_by_id(params[:item][:item_id]) rescue nil
      render json: {status: false, errors: ['Item not  exist.']} and return if item_exist.blank?
      @item = ::Item.find_by_id(params[:item][:item_id])
      @item.features = params[:item][:features]
      if @item.update(add_product_params_with_vendor)
        update_vendor_inventory
        #render json: {status: true, data: {item: @item}}
      else
        errors = @item.errors.messages.each.map {|k,v| v}
        render json: {status: @item, errors: errors.flatten }
      end
    rescue Exception => e  
      render json: {status: false, error: "Item Update failed" ,  error_message:e.message} 
    end
  end

  def update_vendor_inventory
    begin
     item_inventory_exist = ::ItemInventory.find_by_id(params[:item_inventory][:id]) rescue nil
     render json: {status: false, errors: ['Item Inventory Not exist.']} and return if item_inventory_exist.blank?
     
     @item_inventory = ::ItemInventory.find_by_id(params[:item_inventory][:id])
      if @item_inventory.update(inventroy_params_with_user)
        render json: {status: true, data: {item_inventory: @item_inventory}}
      else
        errors = @item_inventory.errors.messages.each.map {|k,v| v}
        render json: {status: false, errors: errors.flatten }
      end
    rescue Exception => e  
      render json: {status: :false, error: "Item Inventory Update failed" ,  error_message:e.message} 
    end
    
  end




  private

  def create_params
    params.require(:vendor).permit(:firstname, :lastname, :email, :password, :mobile_number, :image, :address1, :address2, :city, :state_id, :country_id, :pin_code, :status)
  end

  def login_details_blank?
    params[:email].blank? || params[:password].blank?
  end

  def authenticate_login(vendor)
    return false unless vendor
    vendor.password == PasswordEncryptor.new(params[:password], :salt => vendor.salt).salted.hash
  end

  def add_product_params_with_vendor
    add_product_params.merge({published_by: @current_user.id})
  end

  def add_product_params
    params.require(:item).permit( :name, :image, :slug, :heading, :sub_heading, :brand_id, :menu_id, :category_id, :description, :features, :status)
  end

  def inventroy_params_with_user
    create_invent_params.merge({vendor_id: @current_user.id})
  end

  def create_invent_params
    params.require(:item_inventory).permit(:item_id, :store_id, :price, :mrp, :instock, :discount, :status)
  end
end
