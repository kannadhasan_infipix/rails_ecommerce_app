class Api::ItemInventory::V1::ItemInventoryController < Api::ItemInventory::V1::ApiController
 skip_before_filter :verify_authenticity_token
  before_action :display_menus, :user_account_detail
  def new
    @item_inventory = ::ItemInventory.new
  end

  def index
    @item_inventory = ::ItemInventory.all
  end

  def show
    begin
      @item_inventory = ::ItemInventory.find(params[:id])
    rescue ActiveRecord::RecordNotFound => e
      @item_inventory = []
    end
  end

  def edit
    begin
      @item_inventory = ::ItemInventory.find(params[:id])
    rescue ActiveRecord::RecordNotFound => e
      @item_inventory = []
    end
  end

  def create
    begin
     item_inventory_exist = ::ItemInventory.find_by("item_id=? and vendor_id=? and store_id=?", params[:item_inventory][:item_id],params[:item_inventory][:vendor_id],params[:item_inventory][:store_id]) rescue nil
     render json: {status: false, errors: ['Item Inventory already exist.']} and return if !item_inventory_exist.blank?
      @item_inventory = ::ItemInventory.new(create_params)
      if @item_inventory.save!
        render json: {status: true, data: {item_inventory: @item_inventory}}
      else
        errors = @item_inventory.errors.messages.each.map {|k,v| v}
        render json: {status: false, errors: errors.flatten }
      end
    rescue Exception => e  
      render json: {status: :false, error: "Item Inventory creation failed" ,  error_message:e.message} 
    end
  end

  def update
    begin
      @item_inventory = ::ItemInventory.find(params[:id])
      if @item_inventory.update(create_params)
        render json: {status: true, data: {item_inventory: @item_inventory}}
      else
        errors = @item_inventory.errors.messages.each.map {|k,v| v}
        render json: {status: false, errors: errors.flatten }
      end
    rescue ActiveRecord::RecordNotFound => e
      render json: {status: :false, error: "Item Inventory not found",  error_message:e.message} 
    end
  end

  def update_status
    begin
      @item_inventory = ::Item.ItemInventory(params[:id])
      @item_inventory.status = params[:status]
      if @item_inventory.save!
        flash[:notice] = "Item Status successfully Updated"
      else
        errors = @item_inventory.errors.messages.each.map {|k,v| v}
        render json: {status: false, errors: errors.flatten }
      end
    rescue ActiveRecord::RecordNotFound => e
      render json: {status: :false, error: "Item Inventory not found",  error_message:e.message} 
    end
  end

  def delete
    begin
      @item_inventory = ::ItemInventory.find(params[:id])
      if @item_inventory.destroy
        render json: {status: true, notice: 'Item Inventory is successfully destroyed.'}
      else
        render json: {status: :false, error: "Item Inventory details deleted failed."}
      end 
    rescue ActiveRecord::RecordNotFound => e
      render json: {status: false, error: "Item Inventory not found",  error_message:e.message} 
    end
  end
 
  private
  def inventroy_params_with_user
    create_inventory_params.merge({vendor_id: @current_user.id})
  end

  def create_params
    params.require(:item_inventory).permit(:item_id, :store_id, :price, :mrp, :instock, :status)
  end
end
