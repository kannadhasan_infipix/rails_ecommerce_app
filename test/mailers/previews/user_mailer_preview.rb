# Preview all emails at http://localhost:3000/rails/mailers/user_mailer
class UserMailerPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/user_mailer/send_email_verification_token
  def send_email_verification_token
    UserMailer.send_email_verification_token
  end

end
