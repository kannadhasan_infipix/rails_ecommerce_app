class CreateVendorAccounts < ActiveRecord::Migration
  def change
    create_table :vendor_accounts do |t|
      t.references :vendor, index: true, foreign_key: true
      t.string :acc_number
      t.string :acc_name
      t.string :bank_name
      t.string :bank_branch_name
      t.string :branch_address
      t.string :ifsc_code
      t.string :acc_type
      t.integer :status

      t.timestamps null: false
    end
  end
end
