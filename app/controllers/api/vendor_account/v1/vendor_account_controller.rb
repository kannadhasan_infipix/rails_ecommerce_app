class Api::VendorAccount::V1::VendorAccountController < Api::VendorAccount::V1::ApiController
  skip_before_filter :verify_authenticity_token
  before_action :display_menus, :user_account_detail, :authenticate

  def new
    @vendor_account = ::VendorAccount.new
  end

  def index
    @vendor_account = ::VendorAccount.all

  end

  def show
    begin
      @vendor_account = ::VendorAccount.find(params[:id])
    rescue ActiveRecord::RecordNotFound => e
      @vendor_account = []
    end
  end

  def edit
    begin
      @vendor_account = ::VendorAccount.find(params[:id])
    rescue ActiveRecord::RecordNotFound => e
      @vendor_account = []
    end
  end

  def create
    
    vendor_account_exist = ::VendorAccount.find_by_acc_number(params[:acc_number]) rescue nil
    if !vendor_account_exist.blank?
      respond_to do |format|
        format.html { redirect_to "/api/vendor_account/v1/vendor_account/new", notice: 'Account was Already Exist.' }    
      end
      return false
    end
    @vendor_account = ::VendorAccount.new(vendor_account_with_user_id)
    respond_to do |format|
      if @vendor_account.save!
        format.html { redirect_to "/api/vendor_account/v1/vendor_account/new", notice: 'Account was successfully created.' }
        format.json { render :show, status: :created, location: @vendor_account }
      else
        format.html { render :new }
        format.json { render json: @vendor_account.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    begin
      @vendor_account = ::VendorAccount.find(params[:id])
      if @vendor_account.update(create_params)
        render json: {status: true, data: {vendor_account: @vendor_account}}
      else
        errors = @vendor_account.errors.messages.each.map {|k,v| v}
        render json: {status: false, errors: errors.flatten }
      end
    rescue ActiveRecord::RecordNotFound => e
      render json: {status: :false, error: "Vendor Account not found",  error_message:e.message} 
    end
  end

  def delete
    begin
      @vendor_account = ::VendorAccount.find(params[:id])
      if @vendor_account.destroy
        render json: {status: true, notice: ' Vendor Account is successfully destroyed.'}
      else
        render json: {status: :false, error: "Vendor Account details deleted failed."}
      end 
    rescue ActiveRecord::RecordNotFound => e
      render json: {status: false, error: "Vendor Account not found",  error_message:e.message} 
    end
  end

  def update_status
    begin
      @vendor_account = ::VendorAccount.find(params[:id])
      @vendor_account.status = params[:status]
      if @vendor_account.save!
        flash[:notice] = "Vendor Account Status successfully Updated"
      else
        errors = @vendor_account.errors.messages.each.map {|k,v| v}
        render json: {status: false, errors: errors.flatten }
      end
    rescue ActiveRecord::RecordNotFound => e
      render json: {status: :false, error: " Vendor Account not found",  error_message:e.message} 
    end
  end

  private

  def vendor_account_with_user_id
    create_params.merge({vendor_id: @current_user.id})
  end


  def create_params
    params.permit( :acc_number, :acc_name, :bank_name, :bank_branch_name, :branch_address, :ifsc_code, :acc_type, :status )
  end
end
