class Api::Administrator::V1::AdministratorController < Api::Administrator::V1::ApiController
  skip_before_filter :verify_authenticity_token
  before_action :display_menus, :user_account_detail
  def index
    @administrator = ::Administrator.all
  end

  def show
    begin
      @administrator = ::Administrator.find(params[:id])
    rescue ActiveRecord::RecordNotFound => e
      @administrator = []
    end
  end

  def new
    @administrator = ::Administrator.new
  end

  def edit
    begin
      @administrator = ::Administrator.find(params[:id])
    rescue ActiveRecord::RecordNotFound => e
      @administrator = []
    end
  end

  def create
    begin
      administrator_exist = ::Administrator.find_by_email(params[:administrator][:email]) rescue nil
      render json: {status: false, errors: ['Email already registered.']} and return if !administrator_exist.blank?
      @administrator = ::Administrator.new(create_params)
      if @administrator.save!
        render json: {status: true, data: {administrator: @administrator}}
      else
        errors = @administrator.errors.messages.each.map {|k,v| v}
        render json: {status: false, errors: errors.flatten }
      end
    rescue Exception => e  
      render json: {status: :false, error: "Administrator creation failed" ,  error_message:e.message} 
    end
  end

  def update
     begin
      @administrator = ::Administrator.find(params[:id])
      if @administrator.update(create_params)
        render json: {status: true, data: {administrator: @administrator}}
      else
        errors = @administrator.errors.messages.each.map {|k,v| v}
        render json: {status: false, errors: errors.flatten }
      end
    rescue ActiveRecord::RecordNotFound => e
      render json: {status: :false, error: "Administrator not found",  error_message:e.message} 
    end
  end

  def delete
     begin
      @administrator = ::Administrator.find(params[:id])
      if @administrator.destroy
        render json: {status: true, notice: 'Administrator is successfully destroyed.'}
      else
        render json: {status: :false, error: "Administrator details deleted failed."}
      end 
    rescue ActiveRecord::RecordNotFound => e
      render json: {status: false, error: "Administrator not found",  error_message:e.message} 
    end
  end
  
  def login
    render json: {status: false, errors: ['Email or Password is empty.']} and return if login_details_blank?
    administrator = ::Administrator.find_by_email(params[:email]) rescue nil

    unless administrator
      render json: {status: false, errors: ['No account exists with this email.']} and return
    end
    if authenticate(administrator)
      @current_administrator = administrator
      session[:administrator_id] = administrator.id
      session[:profile_name] = administrator.profile_name
      session[:auth_token] = administrator.auth_token
      #redirect_to api_administrator_v1_administrator_path
    else
      render json: {status: false, errors: ['Email and Password does not match.']} and return
    end
  end

  def logout
    session.delete(:administrator_id)
    session.delete(:administrator_id) 
    session.delete(:profile_name)
    session.delete(:auth_token)
    @current_administrator = nil
    redirect_to api_administrator_v1_administrator_path
  end

  def forgot_password
    begin
      @administrator = ::Administrator.find_by_email(params[:email]) 
      encrypted_password = PasswordEncryptor.new(params[:password]).salted
      @administrator.salt = encrypted_password.salt
      @administrator.password = encrypted_password.hash
      
      if @administrator.save!
        flash[:notice] = "Administrator Password successfully Updated"
        #redirect_to api_administrator_v1_administrator_path
      else
        errors = @administrator.errors.messages.each.map {|k,v| v}
        render json: {status: false, errors: errors.flatten }
      end
    rescue ActiveRecord::RecordNotFound => e
      render json: {status: :false, error: "Administrator not found",  error_message:e.message} 
    end
  end

  def update_status
    begin
      @administrator = ::Administrator.find(params[:id])
      @administrator.status = params[:status]
      
      if @administrator.save!
        flash[:notice] = "Administrator Status successfully Updated"
      else
        errors = @administrator.errors.messages.each.map {|k,v| v}
        render json: {status: false, errors: errors.flatten }
      end
    rescue ActiveRecord::RecordNotFound => e
      render json: {status: :false, error: "Administrator not found",  error_message:e.message} 
    end
  end

  private

  def create_params
    params.require(:administrator).permit(:profile_name, :email, :password, :status)
  end

  def login_details_blank?
    params[:email].blank? || params[:password].blank?
  end

  def authenticate(administrator)
    return false unless administrator
    administrator.password == PasswordEncryptor.new(params[:password], :salt => administrator.salt).salted.hash
  end

end
