class ItemInventory < ActiveRecord::Base
  belongs_to :item
  belongs_to :vendor
  belongs_to :store
  
  enum status: {draft: 0, active: 1, deactive: 2}
end
