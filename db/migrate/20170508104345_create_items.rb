class CreateItems < ActiveRecord::Migration
  def change
    create_table :items do |t|
      t.string :name
      t.string :image
      t.string :heading
      t.string :sub_heading
      t.references :brand, index: true, foreign_key: true
      t.references :menu, index: true, foreign_key: true
      t.references :category, index: true, foreign_key: true
      t.text :description
      t.integer :published_by , default:0
      t.string :slug
      t.jsonb :features, default: '{}'
      t.integer :status , default:0
 
      t.timestamps null: false
    end
  end
end
