class Api::Item::V1::ItemController < Api::Item::V1::ApiController
  skip_before_filter :verify_authenticity_token
  before_action :display_menus, :user_account_detail
  def new
    @item = ::Item.new
  end

  def index
    @item = ::Item.all
    render json: {status: @item}
  end

  def show
    begin
     @item = ::Item.select("items.*").find(params[:id])
     @item_vendors = ::Vendor.select("vendors.id as vendor_id, vendors.firstname,vendors.city ,vendors.pin_code, item_inventories.price,item_inventories.mrp,item_inventories.instock,item_inventories.discount").joins("left JOIN item_inventories ON item_inventories.vendor_id = vendors.id").where("item_inventories.item_id=?",@item.id).order("item_inventories.price DESC").uniq.limit(5)
     @related_item = ::Item.select("items.*, item_inventories.*").joins(:item_inventory).where("items.category_id=?",@item.category_id).limit(5)
     @item_comments = ::Comment.select("comments.*").where("comments.item_id=?",@item.id).order('comments.created_at DESC').limit(6)
     #render json: {status: @related_item }
    rescue ActiveRecord::RecordNotFound => e
      @item = []
    end
  end

  def edit
    begin
      @item = ::Item.find(params[:id])
    rescue ActiveRecord::RecordNotFound => e
      @item = []
    end
  end

  def create
    begin
      item_exist = ::Item.find_by_name(params[:item][:name]) rescue nil
      render json: {status: false, errors: ['Item already exist.']} and return if !item_exist.blank?
      @item = ::Item.new(create_params)
      @item.features = params[:item][:features]
      if @item.save!
        render json: {status: true, data: {item: @item}}
      else
        errors = @item.errors.messages.each.map {|k,v| v}
        render json: {status: false, errors: errors.flatten }
      end
    rescue Exception => e  
      render json: {status: :false, error: "Item creation failed" ,  error_message:e.message} 
    end
  end

  def update
    begin
      @item = ::Item.find(params[:id])
      if @item.update(create_params)
        render json: {status: true, data: {item: @item}}
      else
        errors = @item.errors.messages.each.map {|k,v| v}
        render json: {status: false, errors: errors.flatten }
      end
    rescue ActiveRecord::RecordNotFound => e
      render json: {status: :false, error: "Item not found",  error_message:e.message} 
    end
  end

  def update_image
    begin
      @item = ::Item.find(params[:id])
      @item.image = params[:image]
      
      if @item.save!
       
         redirect_to api_vendor_v1_vendor_show_path
      else
        errors = @item.errors.messages.each.map {|k,v| v}
        render json: {status: false, errors: errors.flatten }
      end
    rescue ActiveRecord::RecordNotFound => e
      render json: {status: :false, error: "User not found",  error_message:e.message} 
    end
  end


  def delete
    begin
      @item = ::Item.find(params[:id])
      if @item.destroy
        render json: {status: true, notice: 'Item is successfully destroyed.'}
      else
        render json: {status: :false, error: "Item details deleted failed."}
      end 
    rescue ActiveRecord::RecordNotFound => e
      render json: {status: false, error: "Item not found",  error_message:e.message} 
    end
  end

  def update_status
    begin
      @item = ::Item.find(params[:id])
      @item.status = params[:status]
      if @item.save!
        flash[:notice] = "Item Status successfully Updated"
      else
        errors = @item.errors.messages.each.map {|k,v| v}
        render json: {status: false, errors: errors.flatten }
      end
    rescue ActiveRecord::RecordNotFound => e
      render json: {status: :false, error: "Item not found",  error_message:e.message} 
    end
  end

  private

  def create_params
    params.require(:item).permit( :name, :published_by, :image, :slug, :heading, :sub_heading, :brand_id, :menu_id, :category_id, :description, :features, :status)
  end
end