class Api::State::V1::StateController < Api::State::V1::ApiController
  skip_before_filter :verify_authenticity_token
  before_action :display_menus, :user_account_detail
  def new
    @state = ::State.new
  end

  def index
    @state = ::State.all
  end

  def show
    begin
      @state = ::State.find(params[:id])
    rescue ActiveRecord::RecordNotFound => e
      @state = []
    end
  end
  def get_state
    begin
      @state = ::State.where("country_id=?",params[:country_id])
      render json: {status: true, data: {state: @state}}
    rescue ActiveRecord::RecordNotFound => e
      @state = []
      render json: {status: false, message:"No State Found"}
    end
  end

  def edit
    begin
      @state = ::State.find(params[:id])
    rescue ActiveRecord::RecordNotFound => e
      @state = []
    end
  end

  def create
    begin
      state_exist = ::State.find_by_name(params[:state][:name]) rescue nil
      render json: {status: false, errors: ['State already exist.']} and return if !state_exist.blank?
      @state = ::State.new(create_params)
      if @state.save!
        render json: {status: true, data: {state: @state}}
      else
        errors = @state.errors.messages.each.map {|k,v| v}
        render json: {status: false, errors: errors.flatten }
      end
    rescue Exception => e  
      render json: {status: :false, error: "State creation failed" ,  error_message:e.message} 
    end
  end

  def update
    begin
      @state = ::State.find(params[:id])
      if @state.update(create_params)
        render json: {status: true, data: {state: @state}}
      else
        errors = @state.errors.messages.each.map {|k,v| v}
        render json: {status: false, errors: errors.flatten }
      end
    rescue ActiveRecord::RecordNotFound => e
      render json: {status: :false, error: "State not found",  error_message:e.message} 
    end
  end

  def delete
    begin
      @state = ::State.find(params[:id])
      if @state.destroy
        render json: {status: true, notice: 'State is successfully destroyed.'}
      else
        render json: {status: :false, error: "State details deleted failed."}
      end 
    rescue ActiveRecord::RecordNotFound => e
      render json: {status: false, error: "State not found",  error_message:e.message} 
    end
  end
 
  private

  def create_params
    params.require(:state).permit(:name,:country_id)
  end
end
