# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170601135546) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "administrators", force: :cascade do |t|
    t.string   "profile_name"
    t.string   "email"
    t.string   "salt"
    t.string   "password"
    t.string   "auth_token"
    t.integer  "status"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "brands", force: :cascade do |t|
    t.string   "name"
    t.string   "image"
    t.string   "description"
    t.string   "slug"
    t.integer  "status"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "categories", force: :cascade do |t|
    t.integer  "menu_id"
    t.string   "name"
    t.integer  "parent_category", default: 0
    t.string   "slug"
    t.integer  "status"
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

  add_index "categories", ["menu_id"], name: "index_categories_on_menu_id", using: :btree

  create_table "comments", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "item_id"
    t.text     "comment_text"
    t.integer  "status"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  add_index "comments", ["item_id"], name: "index_comments_on_item_id", using: :btree
  add_index "comments", ["user_id"], name: "index_comments_on_user_id", using: :btree

  create_table "countries", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "item_inventories", force: :cascade do |t|
    t.integer  "item_id"
    t.integer  "vendor_id"
    t.integer  "store_id"
    t.integer  "price"
    t.integer  "mrp"
    t.integer  "instock"
    t.integer  "discount"
    t.integer  "status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "item_inventories", ["item_id"], name: "index_item_inventories_on_item_id", using: :btree
  add_index "item_inventories", ["store_id"], name: "index_item_inventories_on_store_id", using: :btree
  add_index "item_inventories", ["vendor_id"], name: "index_item_inventories_on_vendor_id", using: :btree

  create_table "item_ratings", force: :cascade do |t|
    t.integer  "item_id"
    t.integer  "user_id"
    t.integer  "rating"
    t.integer  "status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "item_ratings", ["item_id"], name: "index_item_ratings_on_item_id", using: :btree
  add_index "item_ratings", ["user_id"], name: "index_item_ratings_on_user_id", using: :btree

  create_table "items", force: :cascade do |t|
    t.string   "name"
    t.string   "image"
    t.string   "heading"
    t.string   "sub_heading"
    t.integer  "brand_id"
    t.integer  "menu_id"
    t.integer  "category_id"
    t.text     "description"
    t.integer  "published_by", default: 0
    t.string   "slug"
    t.jsonb    "features",     default: {}
    t.integer  "status",       default: 0
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  add_index "items", ["brand_id"], name: "index_items_on_brand_id", using: :btree
  add_index "items", ["category_id"], name: "index_items_on_category_id", using: :btree
  add_index "items", ["menu_id"], name: "index_items_on_menu_id", using: :btree

  create_table "menus", force: :cascade do |t|
    t.string   "name"
    t.string   "slug"
    t.integer  "status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "orders", force: :cascade do |t|
    t.string   "order_id"
    t.integer  "user_id"
    t.integer  "item_id"
    t.integer  "vendor_id"
    t.integer  "order_quantity"
    t.integer  "order_size"
    t.string   "order_color"
    t.datetime "date_of_purchase"
    t.integer  "payment_type"
    t.integer  "payable_amount"
    t.string   "shipping_name"
    t.string   "shipping_address"
    t.string   "shipping_city"
    t.string   "shipping_phone"
    t.string   "shipping_state"
    t.string   "shipping_country"
    t.string   "shipping_pincode"
    t.string   "shipping_landmark"
    t.integer  "status"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
  end

  add_index "orders", ["item_id"], name: "index_orders_on_item_id", using: :btree
  add_index "orders", ["user_id"], name: "index_orders_on_user_id", using: :btree
  add_index "orders", ["vendor_id"], name: "index_orders_on_vendor_id", using: :btree

  create_table "payments", force: :cascade do |t|
    t.string   "session_id"
    t.string   "transaction_id"
    t.string   "order_id"
    t.integer  "amount"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.string   "pgi_id"
    t.string   "status"
  end

  create_table "pets", force: :cascade do |t|
    t.string   "name"
    t.string   "address"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "shopping_carts", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "item_id"
    t.integer  "status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "shopping_carts", ["item_id"], name: "index_shopping_carts_on_item_id", using: :btree
  add_index "shopping_carts", ["user_id"], name: "index_shopping_carts_on_user_id", using: :btree

  create_table "states", force: :cascade do |t|
    t.integer  "country_id"
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "states", ["country_id"], name: "index_states_on_country_id", using: :btree

  create_table "stores", force: :cascade do |t|
    t.integer  "vendor_id"
    t.string   "name"
    t.string   "image"
    t.string   "description"
    t.integer  "status"
    t.string   "slug"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "stores", ["vendor_id"], name: "index_stores_on_vendor_id", using: :btree

  create_table "user_accounts", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "acc_number"
    t.string   "acc_name"
    t.string   "bank_name"
    t.string   "bank_branch_name"
    t.string   "branch_address"
    t.string   "ifsc_code"
    t.string   "acc_type"
    t.integer  "status"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  add_index "user_accounts", ["user_id"], name: "index_user_accounts_on_user_id", using: :btree

  create_table "user_wishlists", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "item_id"
    t.integer  "status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "user_wishlists", ["item_id"], name: "index_user_wishlists_on_item_id", using: :btree
  add_index "user_wishlists", ["user_id"], name: "index_user_wishlists_on_user_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "firstname"
    t.string   "lastname"
    t.string   "email"
    t.string   "password"
    t.string   "salt"
    t.string   "auth_token"
    t.string   "email_verification_token"
    t.boolean  "email_verified",                   default: true
    t.string   "mobile_number"
    t.string   "mobile_number_verification_token"
    t.boolean  "mobile_number_verfied",            default: true
    t.string   "image"
    t.string   "address1"
    t.string   "address2"
    t.string   "city"
    t.integer  "state_id"
    t.integer  "country_id"
    t.string   "pin_code"
    t.integer  "status"
    t.datetime "created_at",                                      null: false
    t.datetime "updated_at",                                      null: false
  end

  add_index "users", ["country_id"], name: "index_users_on_country_id", using: :btree
  add_index "users", ["state_id"], name: "index_users_on_state_id", using: :btree

  create_table "vendor_accounts", force: :cascade do |t|
    t.integer  "vendor_id"
    t.string   "acc_number"
    t.string   "acc_name"
    t.string   "bank_name"
    t.string   "bank_branch_name"
    t.string   "branch_address"
    t.string   "ifsc_code"
    t.string   "acc_type"
    t.integer  "status"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  add_index "vendor_accounts", ["vendor_id"], name: "index_vendor_accounts_on_vendor_id", using: :btree

  create_table "vendors", force: :cascade do |t|
    t.string   "firstname"
    t.string   "lastname"
    t.string   "email"
    t.string   "password"
    t.string   "salt"
    t.string   "auth_token"
    t.string   "email_verification_token"
    t.boolean  "email_verified",                   default: true
    t.string   "mobile_number"
    t.string   "mobile_number_verification_token"
    t.boolean  "mobile_number_verfied",            default: true
    t.string   "image"
    t.string   "address1"
    t.string   "address2"
    t.string   "city"
    t.integer  "state_id"
    t.integer  "country_id"
    t.string   "pin_code"
    t.integer  "status"
    t.datetime "created_at",                                      null: false
    t.datetime "updated_at",                                      null: false
  end

  add_index "vendors", ["country_id"], name: "index_vendors_on_country_id", using: :btree
  add_index "vendors", ["state_id"], name: "index_vendors_on_state_id", using: :btree

  add_foreign_key "categories", "menus"
  add_foreign_key "comments", "items"
  add_foreign_key "comments", "users"
  add_foreign_key "item_inventories", "items"
  add_foreign_key "item_inventories", "stores"
  add_foreign_key "item_inventories", "vendors"
  add_foreign_key "item_ratings", "items"
  add_foreign_key "item_ratings", "users"
  add_foreign_key "items", "brands"
  add_foreign_key "items", "categories"
  add_foreign_key "items", "menus"
  add_foreign_key "orders", "items"
  add_foreign_key "orders", "users"
  add_foreign_key "orders", "vendors"
  add_foreign_key "shopping_carts", "items"
  add_foreign_key "shopping_carts", "users"
  add_foreign_key "states", "countries"
  add_foreign_key "stores", "vendors"
  add_foreign_key "user_accounts", "users"
  add_foreign_key "user_wishlists", "items"
  add_foreign_key "user_wishlists", "users"
  add_foreign_key "users", "countries"
  add_foreign_key "users", "states"
  add_foreign_key "vendor_accounts", "vendors"
  add_foreign_key "vendors", "countries"
  add_foreign_key "vendors", "states"
end
