class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  def display_menus
    menu_list = HomeMenuBuilder.new
    @home_men_category = menu_list.get_menu_item_list(1)
    @home_women_category = menu_list.get_menu_item_list(2)
  end

  def user_account_detail
    
    @cart_count= 0
    @cart_item_amount= 0
    if !user_id_by_auth_token.blank?
     get_count = ::ShoppingCart.select("item_id").where("user_id=? and status=1",user_id_by_auth_token)
     get_amount = ::ShoppingCart.select("sum(price) as amount").joins(item: :item_inventory).find_by_user_id(user_id_by_auth_token)
     @cart_count= get_count.count
     @cart_item_amount= get_amount.amount
     @user_wishlist = ::UserWishlist.select("item_id").where("user_id=? and status=1",user_id_by_auth_token)
    end
    @wishlist_item = Array.new
    if !@user_wishlist.blank?
      @user_wishlist.each do |item,value| 
      @wishlist_item << item["item_id"]
    end
    end
    #render json: {user_wishlist_item: @wishlist_item, in_cart: @cart_count, auth_token: cookies[:user_auth_token],user: user_id_by_auth_token}
  end

  def user_id_by_auth_token
    if !cookies[:user_auth_token].blank?
      @current_user = ::User.find_by_auth_token(cookies[:user_auth_token])
      @current_user.id
    end
  end
end
