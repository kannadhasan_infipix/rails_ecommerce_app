class CreateCategories < ActiveRecord::Migration
  def change
    create_table :categories do |t|
      t.references :menu, index: true, foreign_key: true
      t.string :name
      t.integer :parent_category, null: true, default: "NULL"
      t.string :slug
      t.integer :status

      t.timestamps null: false
    end
  end
end
