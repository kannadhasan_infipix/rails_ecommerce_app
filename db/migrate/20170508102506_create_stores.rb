class CreateStores < ActiveRecord::Migration
  def change
    create_table :stores do |t|
      t.references :vendor, index: true, foreign_key: true
      t.string :name
      t.string :image
      t.string :description
      t.integer :status
      t.string :slug

      t.timestamps null: false
    end
  end
end
