class Api::Brand::V1::BrandController < Api::Brand::V1::ApiController
  skip_before_filter :verify_authenticity_token
  before_action :display_menus, :user_account_detail
  def new
    @brand = ::Brand.new
  end

  def index
    @brand = ::Brand.all
  end

  def show
    begin
      @brand = ::Brand.find(params[:id])
    rescue ActiveRecord::RecordNotFound => e
      @brand = []
    end
  end

  def edit
    begin
      @brand = ::Brand.find(params[:id])
    rescue ActiveRecord::RecordNotFound => e
      @brand = []
    end
  end

  def create
    begin
      brand_exist = ::Brand.find_by_name(params[:brand][:name]) rescue nil
      render json: {status: false, errors: ['Brand already exist.']} and return if !brand_exist.blank?
      @brand = ::Brand.new(create_params)
      if @brand.save!
        render json: {status: true, data: {brand: @brand}}
      else
        errors = @brand.errors.messages.each.map {|k,v| v}
        render json: {status: false, errors: errors.flatten }
      end
    rescue Exception => e  
      render json: {status: :false, error: "Brand creation failed" ,  error_message:e.message} 
    end
  end

  def update
    begin
      @brand = ::Brand.find(params[:id])
      if @brand.update(create_params)
        render json: {status: true, data: {brand: @brand}}
      else
        errors = @brand.errors.messages.each.map {|k,v| v}
        render json: {status: false, errors: errors.flatten }
      end
    rescue ActiveRecord::RecordNotFound => e
      render json: {status: :false, error: "Brand not found",  error_message:e.message} 
    end
  end

  def delete
    begin
      @brand = ::Brand.find(params[:id])
      if @brand.destroy
        render json: {status: true, notice: 'Brand is successfully destroyed.'}
      else
        render json: {status: :false, error: "Brand details deleted failed."}
      end 
    rescue ActiveRecord::RecordNotFound => e
      render json: {status: false, error: "Brand not found",  error_message:e.message} 
    end
  end

  def update_status
    begin
      @brand = ::Brand.find(params[:id])
      @brand.status = params[:status]
      if @brand.save!
        flash[:notice] = "Brand Status successfully Updated"
      else
        errors = @brand.errors.messages.each.map {|k,v| v}
        render json: {status: false, errors: errors.flatten }
      end
    rescue ActiveRecord::RecordNotFound => e
      render json: {status: :false, error: "brand not found",  error_message:e.message} 
    end
  end
  def update_image
    begin
      @brand = ::Brand.find(params[:id])
      @brand.image = params[:image]
      if @brand.save!
        flash[:notice] = "Brand Image successfully Updated"
      else
        errors = @brand.errors.messages.each.map {|k,v| v}
        render json: {status: false, errors: errors.flatten }
      end
    rescue ActiveRecord::RecordNotFound => e
      render json: {status: :false, error: "Brand not found",  error_message:e.message} 
    end
  end

  private

  def create_params
    params.require(:brand).permit(:name, :slug, :image, :description, :status)
  end
end
