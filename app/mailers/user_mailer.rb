class UserMailer < ApplicationMailer
  def send_email_verification_token(user)
    @user = user
    mail(to: @user.email, subject: 'Online shopping - Email verification')
  end
end
