class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :firstname
      t.string :lastname
      t.string :email
      t.string :password
      t.string :salt
      t.string :auth_token
      t.string :email_verification_token
      t.boolean :email_verified , default: true
      t.string :mobile_number
      t.string :mobile_number_verification_token
      t.boolean :mobile_number_verfied , default: true
      t.string :image
      t.string :address1
      t.string :address2
      t.string :city
      t.references :state, index: true, foreign_key: true
      t.references :country, index: true, foreign_key: true
      t.string :pin_code
      t.integer :status
      t.timestamps null: false
    end
  end
end
