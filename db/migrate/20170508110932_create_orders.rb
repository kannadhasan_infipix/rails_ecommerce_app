class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.string :order_id
      t.references :user, index: true, foreign_key: true
      t.references :item, index: true, foreign_key: true
      t.references :vendor, index: true, foreign_key: true
      t.integer :order_quantity
      t.integer :order_size
      t.string :order_color
      t.datetime :date_of_purchase
      t.integer :payment_type
      t.integer :payable_amount
      t.string :shipping_name
      t.string :shipping_address
      t.string :shipping_city
      t.string :shipping_phone
      t.string :shipping_state
      t.string :shipping_country
      t.string :shipping_pincode
      t.string :shipping_landmark
      t.integer :status

      t.timestamps null: false
    end
  end
end
