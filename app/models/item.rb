class Item < ActiveRecord::Base
  belongs_to :brand
  belongs_to :menu
  belongs_to :category
  has_many :item_inventory
  has_many :ShoppingCart
  has_many :UserWishlist
  has_many :Order
  enum status: {draft: 0, active: 1, deactive: 2}
  mount_uploader :image, ItemAvatarUploader
  
end
