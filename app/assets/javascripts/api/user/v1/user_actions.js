$(document).ready(function(){
    
  $("#user_login").submit(function( e ) {

    var user_login_form = $(this).serialize();
    $.ajax({
        type: "POST",
        url: "http://localhost:3000/api/user/v1/user/login",
        data: user_login_form,
        cache: false,
        dataType: "json",
        success: function(response){
          console.log(response);
          if(response.status){
            alert("Login Successful");
            $(".profile_tag").show();
            $(".topbar_login").hide();
            $(".user_cart_amount").text(response.useraccount.cart_amount);
            $(".user_cart_count").text(response.useraccount.cart_count);
            $("#view_cart").attr("href","/api/shopping_cart/v1/shopping_cart/");
            $("#user_login").trigger("reset");
            $("#close_login_modal").trigger("click");
          }else{
            alert(response.errors)
          }
        },
        error: function(xhr, status, error) {
          alert(xhr.status+"  "+error)
        }
      });
    e.preventDefault();
  });

  // $("#user_create").submit(function( e ) {
  //   var user_create_form = {};
  //   $(this).serializeArray().map(function(x){user_create_form[x.name] = x.value;}); 
  //   $.ajax({
  //       type: "POST",
  //       url: "/api/user/v1/user/create",
  //       data: {"user": user_create_form},
  //       cache: false,
  //       dataType: "json",
  //       success: function(response){
  //         console.log(response);
  //         if(response.status){
  //           var today = new Date();
  //           var expr = new Date(today.getTime() + 1 * 24 * 60 * 60 * 1000);
  //           document.cookie = "user_auth_token="+response.user_auth_token+"; expires=" + expr.toGMTString();
  //           alert("Registered Successfully");
  //           window.location.href="/"; 
  //         }else{
  //           alert(response.errors)
  //         }
  //       }
  //     });
  //   e.preventDefault();
  // });

});