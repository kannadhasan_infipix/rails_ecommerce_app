class UserPaymentMailer < ApplicationMailer
  def send_payment_information(email)
    mail(to: email, subject: 'Online shopping - Payment Information')
  end
end
