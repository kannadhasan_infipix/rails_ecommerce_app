class Api::Menu::V1::MenuController < Api::Menu::V1::ApiController
  skip_before_filter :verify_authenticity_token
  before_action :display_menus, :user_account_detail
  def new
    @menu = ::Menu.new
  end

  def index
    @menu = ::Menu.all
  end

  def show
    begin
      @menu = ::Menu.find(params[:id])
    rescue ActiveRecord::RecordNotFound => e
      @menu = []
    end
  end

  def edit
    begin
      @menu = ::Menu.find(params[:id])
    rescue ActiveRecord::RecordNotFound => e
      @menu = []
    end
  end

  def create
    begin
      menu_exist = ::Menu.find_by_name(params[:menu][:name]) rescue nil
      render json: {status: false, errors: ['Menu already exist.']} and return if !menu_exist.blank?
      @menu = ::Menu.new(create_params)
      if @menu.save!
        render json: {status: true, data: {menu: @menu}}
      else
        errors = @menu.errors.messages.each.map {|k,v| v}
        render json: {status: false, errors: errors.flatten }
      end
    rescue Exception => e  
      render json: {status: :false, error: "Menu creation failed" ,  error_message:e.message} 
    end
  end

  def update
    begin
      @menu = ::Menu.find(params[:id])
      if @menu.update(create_params)
        render json: {status: true, data: {menu: @menu}}
      else
        errors = @menu.errors.messages.each.map {|k,v| v}
        render json: {status: false, errors: errors.flatten }
      end
    rescue ActiveRecord::RecordNotFound => e
      render json: {status: :false, error: "Menu not found",  error_message:e.message} 
    end
  end

  def delete
    begin
      @menu = ::Menu.find(params[:id])
      if @menu.destroy
        render json: {status: true, notice: 'Menu is successfully destroyed.'}
      else
        render json: {status: :false, error: "Menu details deleted failed."}
      end 
    rescue ActiveRecord::RecordNotFound => e
      render json: {status: false, error: "Menu not found",  error_message:e.message} 
    end
  end

  def update_status
    begin
      @menu = ::Menu.find(params[:id])
      @menu.status = params[:status]
      if @menu.save!
        flash[:notice] = "Menu Status successfully Updated"
      else
        errors = @menu.errors.messages.each.map {|k,v| v}
        render json: {status: false, errors: errors.flatten }
      end
    rescue ActiveRecord::RecordNotFound => e
      render json: {status: :false, error: "menu not found",  error_message:e.message} 
    end
  end

  private

  def create_params
    params.require(:menu).permit(:name, :slug, :parent_id :status)
  end


end
