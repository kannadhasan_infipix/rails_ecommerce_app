class Api::Store::V1::StoreController < Api::Store::V1::ApiController
  skip_before_filter :verify_authenticity_token
  before_action :display_menus, :user_account_detail
  def new
    @store = ::Store.new
  end

  def index
    @store = ::Store.all
  end

  def show
    begin
      @store = ::Store.find(params[:id])
    rescue ActiveRecord::RecordNotFound => e
      @store = []
    end
  end

  def edit
    begin
      @store = ::Store.find(params[:id])
    rescue ActiveRecord::RecordNotFound => e
      @store = []
    end
  end

  def create
    begin
      store_exist = ::Store.find_by_name(params[:store][:name]) rescue nil
      render json: {status: false, errors: ['Store already exist.']} and return if !store_exist.blank?
      @store = ::Store.new(create_params)
      if @store.save!
        render json: {status: true, data: {store: @store}}
      else
        errors = @store.errors.messages.each.map {|k,v| v}
        render json: {status: false, errors: errors.flatten }
      end
    rescue Exception => e  
      render json: {status: :false, error: "Store creation failed" ,  error_message:e.message} 
    end
  end

  def update
    begin
      @store = ::Store.find(params[:id])
      if @store.update(create_params)
        render json: {status: true, data: {store: @store}}
      else
        errors = @store.errors.messages.each.map {|k,v| v}
        render json: {status: false, errors: errors.flatten }
      end
    rescue ActiveRecord::RecordNotFound => e
      render json: {status: :false, error: "Store not found",  error_message:e.message} 
    end
  end

  def delete
    begin
      @store = ::Store.find(params[:id])
      if @store.destroy
        render json: {status: true, notice: 'Store is successfully destroyed.'}
      else
        render json: {status: :false, error: "Store details deleted failed."}
      end 
    rescue ActiveRecord::RecordNotFound => e
      render json: {status: false, error: "Store not found",  error_message:e.message} 
    end
  end

  def update_status
    begin
      @store = ::Store.find(params[:id])
      @store.status = params[:status]
      if @store.save!
        flash[:notice] = "Store Status successfully Updated"
      else
        errors = @store.errors.messages.each.map {|k,v| v}
        render json: {status: false, errors: errors.flatten }
      end
    rescue ActiveRecord::RecordNotFound => e
      render json: {status: :false, error: "Store not found",  error_message:e.message} 
    end
  end
  def update_image
    begin
      @store = ::Store.find(params[:id])
      @store.image = params[:image]
      if @store.save!
        flash[:notice] = "Store Image successfully Updated"
      else
        errors = @store.errors.messages.each.map {|k,v| v}
        render json: {status: false, errors: errors.flatten }
      end
    rescue ActiveRecord::RecordNotFound => e
      render json: {status: :false, error: "Store not found",  error_message:e.message} 
    end
  end

  private

  def create_params
    params.require(:store).permit(:name, :slug, :vendor_id, :image, :description, :status)
  end
end
