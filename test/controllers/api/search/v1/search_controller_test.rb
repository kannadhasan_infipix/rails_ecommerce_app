require 'test_helper'

class Api::Search::V1::SearchControllerTest < ActionController::TestCase
  test "should get by_menu" do
    get :by_menu
    assert_response :success
  end

  test "should get by_category" do
    get :by_category
    assert_response :success
  end

end
