class HomeMenuBuilder

  def menu_list(menu_id)
    category= ::Category.select("categories.id,categories.name").where("parent_category=? and menu_id=?",0,menu_id)
    build_category_and_sub_category_menu_list(category)
  end

  def build_category_and_sub_category_menu_list(category)
    build_menu = Hash.new 
    category.each do |element|
      build_menu[element.name]= subcategory = ::Category.select("categories.id, categories.name, categories.slug").where("parent_category=? ",element.id)
    end
    build_menu
  end

  def get_menu_item_list(menu_id)
    menu_list(menu_id)
  end

end