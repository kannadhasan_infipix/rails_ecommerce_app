class Store < ActiveRecord::Base
  belongs_to :Vendor
  mount_uploader :image, StoreImageUploderUploader 
end
