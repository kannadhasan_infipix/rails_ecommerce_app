class CreateItemRatings < ActiveRecord::Migration
  def change
    create_table :item_ratings do |t|
      t.references :item, index: true, foreign_key: true
      t.references :user, index: true, foreign_key: true
      t.integer :rating
      t.integer :status

      t.timestamps null: false
    end
  end
end
