class User < ActiveRecord::Base

  
  enum status: {draft: 0, active: 1, deactive: 2}
  belongs_to :state
  belongs_to :country
  has_many :order
  #has_many :user_account
  #has_many :item_inventory

  before_create :assign_salt_and_hashed_password
  before_create :assign_auth_token
  before_create :assign_email_verification_token
  before_create :assign_phone_verification_token
  mount_uploader :image, UserAvatarUploader

  private
  
  def assign_auth_token
    self.auth_token = generate_auth_token
  end

  def assign_salt_and_hashed_password
    encrypted_password = PasswordEncryptor.new(self.password).salted
    self.salt = encrypted_password.salt
    self.password = encrypted_password.hash
  end

  def assign_email_verification_token
    self.email_verification_token = generate_email_verification_token
  end

  def assign_phone_verification_token
    self.mobile_number_verification_token = PhoneVerificationTokenGenerator.new(self.mobile_number).generate_token
  end

  def generate_email_verification_token
    loop do
      token = EmailVerificationTokenGenerator.new(self.email).generate_token
      break token unless User.exists?(email_verification_token: token)
    end
  end

  def generate_auth_token
    loop do
      token = SecureRandom.base64.tr('+/=', self.email)
      break token unless User.exists?(auth_token: token)
    end
  end
end
