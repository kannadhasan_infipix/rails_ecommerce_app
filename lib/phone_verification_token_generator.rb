class PhoneVerificationTokenGenerator
  
  def initialize(phone)
    @token = rand(1000...9999)
  end

  def generate_new_salt
    BCrypt::Engine.generate_salt
  end

  def generate_token
    return @token
  end
end