Rails.application.routes.draw do
  root 'home#index'
  mount RailsAdmin::Engine => '/super_admin', as: 'rails_admin'
  namespace :api do
    namespace :administrator do
      namespace :v1 do
        get 'administrator/' => 'administrator#index'
        get 'administrator/:id/show' => 'administrator#show'
        get 'administrator/:id/edit' => 'administrator#edit'
        post 'administrator/create' => 'administrator#create'
        post 'administrator/:id/update' => 'administrator#update'
        get 'administrator/:id/delete' => 'administrator#delete'
        post 'administrator/login' => 'administrator#login'
        get 'administrator/logout' => 'administrator#logout'
        post 'administrator/forgot_password' => 'administrator#forgot_password'
        get 'administrator/:id/update_status' => 'administrator#update_status'
      end
    end
  end

  namespace :api do
    namespace :menu do
      namespace :v1 do
        get 'menu/' => 'menu#index'
        get 'menu/new' => 'menu#new'
        get 'menu/:id/show' => 'menu#show'
        get 'menu/:id/edit' => 'menu#edit'
        post 'menu/create' => 'menu#create'
        post 'menu/:id/update' => 'menu#update'
        get 'menu/:id/delete' => 'menu#delete'
        get 'menu/:id/update_status' => 'menu#update_status'
      end
    end
  end

  namespace :api do
    namespace :category do
      namespace :v1 do
        get 'category/' => 'category#index'
        get 'category/new' => 'category#new'
        get 'category/:slug/show' => 'category#show'
        get 'category/:id/edit' => 'category#edit'
        post 'category/create' => 'category#create'
        post 'category/:id/update' => 'category#update'
        get 'category/:id/delete' => 'category#delete'
        get 'category/:id/update_status' => 'category#update_status'
      end
    end
  end

  namespace :api do
    namespace :brand do
      namespace :v1 do
        get 'brand/' => 'brand#index'
        get 'brand/new' => 'brand#new'
        get 'brand/:id/show' => 'brand#show'
        get 'brand/:id/edit' => 'brand#edit'
        post 'brand/create' => 'brand#create'
        post 'brand/:id/update' => 'brand#update'
        get 'brand/:id/delete' => 'brand#delete'
        get 'brand/:id/update_status' => 'brand#update_status'
        post 'brand/:id/update_status' => 'brand#update_image'
      end
    end
  end

  namespace :api do
    namespace :store do
      namespace :v1 do
        get 'store/' => 'store#index'
        get 'store/new' => 'store#new'
        get 'store/:id/show' => 'store#show'
        get 'store/:id/edit' => 'store#edit'
        post 'store/create' => 'store#create'
        post 'store/:id/update' => 'store#update'
        delete 'store/:id/delete' => 'store#delete'
        get 'store/:id/update_status' => 'store#update_status'
        post 'store/:id/update_status' => 'store#update_image'

      end
    end
  end

  namespace :api do
    namespace :country do
      namespace :v1 do
        get 'country/' =>  'country#index'
        get 'country/new' =>  'country#new'
        get 'country/:id/show' =>  'country#show'
        get 'country/:id/edit' =>  'country#edit'
        post 'country/create' =>  'country#create'
        post 'country/:id/update' =>  'country#update'
        get 'country/:id/delete' =>  'country#delete'
      end
    end
  end

  namespace :api do
    namespace :state do
      namespace :v1 do
        get 'state/' =>  'state#index'
        get 'state/new' =>  'state#new'
        get 'state/:id/show' =>  'state#show'
        get 'state/:id/edit' =>  'state#edit'
        post 'state/create' =>  'state#create'
        post 'state/:id/update' =>  'state#update'
        get 'state/:id/delete' =>  'state#delete'
        get 'state/get_state' =>  'state#get_state'
        
      end
    end
  end

  namespace :api do
    namespace :vendor do
      namespace :v1 do
        get 'vendor/' =>  'vendor#index'
        get 'vendor/new' =>  'vendor#new'
        get 'vendor/show' =>  'vendor#show'
        post 'vendor/create' =>  'vendor#create'
        post 'vendor/:id/update' =>  'vendor#update'
        get 'vendor/:id/delete' =>  'vendor#delete'
        get 'vendor/:id/update_status' =>  'vendor#update_status'
        post 'vendor/:id/update_image' =>  'vendor#update_image'
        post 'vendor/login' =>  'vendor#login'
        get 'vendor/logout' =>  'vendor#logout'
        post 'vendor/forgot_password' =>  'vendor#forgot_password'
        get 'vendor/:id/vendor_order' =>  'vendor#vendor_order'
        post 'vendor/my_product' =>  'vendor#add_product'
        post 'vendor/my_inventory' =>  'vendor#add_inventory'
        get 'vendor/:id/edit_product' =>  'vendor#edit_vendor_product'
        post 'vendor/update_my_product' =>  'vendor#update_vendor_product'
        post 'vendor/update_my_inventory' =>  'vendor#update_vendor_inventory'
      end
    end
  end

  namespace :api do
    namespace :vendor_account do
      namespace :v1 do
        get 'vendor_account/' =>  'vendor_account#index'
        get 'vendor_account/new' =>  'vendor_account#new'
        get 'vendor_account/:id/show' =>  'vendor_account#show'
        get 'vendor_account/:id/edit' =>  'vendor_account#edit'
        post 'vendor_account/create' =>  'vendor_account#create'
        post 'vendor_account/:id/update' =>  'vendor_account#update'
        get 'vendor_account/:id/delete' =>  'vendor_account#delete'
        get 'vendor_account/:id/update_status' =>  'vendor_account#update_status'


      end
    end
  end

  namespace :api do
    namespace :user do
      namespace :v1 do
        get 'user/new' =>  'user#new'
        get 'user/' =>  'user#index'
        get 'user/:id/show' =>  'user#show'
        post 'user/create' =>  'user#create'
        post 'user/:id/update' =>  'user#update'
        get 'user/:id/delete' =>  'user#delete'
        get 'user/:id/update_status' =>  'user#update_status'
        post 'user/:id/update_image' =>  'user#update_image'
        post 'user/login' =>  'user#login'
        get 'user/logout' =>  'user#logout'
        post 'user/forgot_password' =>  'user#forgot_password'
        get 'user/verify_email' =>  'user#verify_email'
      end
    end
  end

  namespace :api do
    namespace :user_account do
      namespace :v1 do
        get 'user_account/' =>  'user_account#index'
        get 'user_account/new' =>  'user_account#new'
        get 'user_account/:id/show' =>  'user_account#show'
        get 'user_account/:id/edit' =>  'user_account#edit'
        post 'user_account/create' =>  'user_account#create'
        post 'user_account/:id/update' =>  'user_account#update'
        get 'user_account/:id/delete' =>  'user_account#delete'
        get 'user_account/:id/update_status' =>  'user_account#update_status'
        
      end
    end
  end

  namespace :api do
    namespace :item do
      namespace :v1 do
        get 'item/new' =>  'item#new'
        get 'item/' =>  'item#index'
        get 'item/:id/show' =>  'item#show'
        get 'item/:id/edit' =>  'item#edit'
        post 'item/create' =>  'item#create'
        post 'item/:id/update' =>  'item#update'
        get 'item/:id/update_status' =>  'item#update_status'
        get 'item/:id/delete' =>  'item#delete'
        post 'item/update_image' =>  'item#update_image'

        
      end
    end
  end

  namespace :api do
    namespace :item_inventory do
      namespace :v1 do
        get 'item_inventory/new' =>  'item_inventory#new'
        get 'item_inventory/' =>  'item_inventory#index'
        get 'item_inventory/:id/show' =>  'item_inventory#show'
        get 'item_inventory/:id/edit' =>  'item_inventory#edit'
        post 'item_inventory/create' =>  'item_inventory#create'
        post 'item_inventory/:id/update' =>  'item_inventory#update'
        get 'item_inventory/:id/update_status' =>  'item_inventory#update_status'
        get 'item_inventory/:id/delete' =>  'item_inventory#delete'
      end
    end
  end

  namespace :api do
    namespace :shopping_cart do
      namespace :v1 do
        get 'shopping_cart/new' =>  'shopping_cart#new'
        get 'shopping_cart/' =>  'shopping_cart#index'
        get 'shopping_cart/:id/show' =>  'shopping_cart#show'
        post 'shopping_cart/create' =>  'shopping_cart#create'
        post 'shopping_cart/:id/update' =>  'shopping_cart#update'
        get 'shopping_cart/delete' =>  'shopping_cart#delete'
        get 'shopping_cart/:id/update_status' =>  'shopping_cart#update_status'
      end
    end
  end

  namespace :api do
    namespace :wishlist do
      namespace :v1 do
        get 'wishlist/new' =>  'wishlist#new'
        get 'wishlist/' =>  'wishlist#index'
        get 'wishlist/:id/show' =>  'wishlist#show'
        post 'wishlist/create' =>  'wishlist#create'
        post 'wishlist/update' =>  'wishlist#update'
        get 'wishlist/delete' =>  'wishlist#delete'
        get 'wishlist/:id/update_status' =>  'wishlist#update_status'
      end
    end
  end

  namespace :api do
    namespace :order do
      namespace :v1 do
        get 'order/' =>  'order#index'
        get 'order/new' =>  'order#new'
        get 'order/:id/show' =>  'order#show'
        post 'order/create' =>  'order#create'
        post 'order/:id/update' =>  'order#update'
        get 'order/:id/delete' =>  'order#delete'
        get 'order/:id/update_status' =>  'order#update_status'
      end
    end
  end

  namespace :api do
    namespace :item_rating do
      namespace :v1 do
        get 'rating/index' =>  'rating#index'
        post 'rating/create' =>  'rating#create'
        get 'rating/new' =>  'rating#new'
        post 'rating/:id/update' =>  'rating#update'
        get 'rating/:id/show' =>  'rating#show'
        get 'rating/:id/delete' =>  'rating#delete'
        get 'rating/:id/update_status' =>  'rating#update_status'
      end
    end
  end


  namespace :api do
    namespace :comment do
      namespace :v1 do
        get 'comment/index' =>  'comment#index'
        post 'comment/create' =>  'comment#create'
        get 'comment/new' =>  'comment#new'
        get 'comment/:id/show' =>  'comment#show'
        post 'comment/:id/update' =>  'comment#update'
        get 'comment/:id/delete' =>  'comment#delete'
        get 'comment/:id/update_status' =>  'comment#update_status'
      end
    end
  end

  namespace :api do
  namespace :search do
    namespace :v1 do
      get 'search/:slug/category' =>  'search#search_by_category'
      get 'search/:category_name/category_name' =>  'search#search_by_category_name'
      get 'search/brand' =>  'search#search_brand_name'
      get 'search/menu' =>  'search#search_menu_name'
      get 'search/category' =>  'search#search_category_name'
      get 'search/product' =>  'search#search_item_name'
      get 'search/store' =>  'search#search_store_name'
      get 'search/product_by_id' =>  'search#search_item_by_id'
      end
    end
  end

  namespace :api do
  namespace :payment do
    namespace :v1 do
      get 'payment/new' => 'payment#new'
      get 'payment/index' => 'payment#index'
      get 'payment/show' => 'payment#show'
      post 'payment/create' => 'payment#create'
      post 'payment/update' => 'payment#update'
      get 'payment/:id/delete' => 'payment#delete'
      get 'payment/update_status' => 'payment#update_status'
      end
    end
  end



end
