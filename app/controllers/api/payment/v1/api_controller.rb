class Api::Payment::V1::ApiController < ApplicationController
  respond_to :json
  protect_from_forgery with: :null_session, if: Proc.new { |c| c.request.format =~ %r{application/json} }
  rescue_from ActiveRecord::RecordNotFound, with: :not_found

  private

  def authenticate
    @current_user = ::User.find_by_auth_token("k9f4p78BkSNGyU23vezIDwmm")
    unless @current_user
      render html: "<script>alert('Please Login!'); window.location='/';</script>".html_safe
    end
  end
end
