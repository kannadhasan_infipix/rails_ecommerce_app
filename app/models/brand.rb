class Brand < ActiveRecord::Base
#has_many :item
mount_uploader :image, BrandImageUploderUploader 
enum status: {draft: 0, active: 1, deactive: 2}
end
