class Api::Comment::V1::CommentController < Api::Comment::V1::ApiController
  skip_before_filter :verify_authenticity_token
  before_action :display_menus, :user_account_detail, :except => [:create]
  before_action :authenticate
  def new
    @comment = ::Comment.new
  end

  def index
    @comment = ::Comment.all
  end

  def show
    begin
      @comment = ::Comment.find(params[:id])
    rescue ActiveRecord::RecordNotFound => e
      @comment = []
    end
  end

  def create
    begin

      @comment = ::Comment.new(comment_params_with_user)
      if @comment.save!
        render json: {status: true, data: {comment: @comment}}
      else
        errors = @comment.errors.messages.each.map {|k,v| v}
        render json: {status: false, errors: errors.flatten }
      end
    rescue Exception => e  
      render json: {status: :false, error: "Comment creation failed" ,  error_message:e.message} 
    end
  end

  def update
    begin
      @comment = ::Comment.find(params[:id])
      if @comment.update(comment_params_with_user)
        render json: {status: true, data: {comment: @comment}}
      else
        errors = @comment.errors.messages.each.map {|k,v| v}
        render json: {status: false, errors: errors.flatten }
      end
    rescue ActiveRecord::RecordNotFound => e
      render json: {status: :false, error: "Comment not found",  error_message:e.message} 
    end
  end

  def delete
    begin
      @comment = ::Comment.find(params[:id])
      if @comment.destroy
        render json: {status: true, notice: 'Comment is successfully destroyed.'}
      else
        render json: {status: :false, error: "Comment details deleted failed."}
      end 
    rescue ActiveRecord::RecordNotFound => e
      render json: {status: false, error: "Comment not found",  error_message:e.message} 
    end
  end

  def update_status
    begin
      @comment = ::Comment.find(params[:id])
      @comment.status = params[:status]
      if @comment.save!
        flash[:notice] = "Comment Status successfully Updated"
      else
        errors = @comment.errors.messages.each.map {|k,v| v}
        render json: {status: false, errors: errors.flatten }
      end
    rescue ActiveRecord::RecordNotFound => e
      render json: {status: :false, error: "Comment not found",  error_message:e.message} 
    end
  end

  private

  def comment_params_with_user
    create_params.merge({user_id: @current_user.id})
  end

  def create_params
    params.require(:comment).permit(:item_id,  :comment_text, :status)
  end
end
