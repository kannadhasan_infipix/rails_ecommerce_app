class Api::ItemRating::V1::RatingController < Api::ItemRating::V1::ApiController 
  skip_before_filter :verify_authenticity_token
  before_action :display_menus, :user_account_detail
  def new
    @item_rating = ::ItemRating.new
  end

  def index
    @item_rating = ::ItemRating.all
  end

  def show
    begin
      @item_rating = ::ItemRating.find(params[:id])
    rescue ActiveRecord::RecordNotFound => e
      @item_rating = []
    end
  end

  def create
    begin

      item_rating_exist = ::ItemRating.find_by("user_id=? and item_id=?",params[:item_rating][:user_id], params[:item_rating][:item_id]) rescue nil
      render json: {status: false, errors: ['Item Rating already exist.']} and return if !item_rating_exist.blank?
      @item_rating = ::ItemRating.new(create_params)
      if @item_rating.save!
        render json: {status: true, data: {item_rating: @item_rating}}
      else
        errors = @item_rating.errors.messages.each.map {|k,v| v}
        render json: {status: false, errors: errors.flatten }
      end
    rescue Exception => e  
      render json: {status: :false, error: "Item Rating creation failed" ,  error_message:e.message} 
    end
  end

  def update
    begin
      @item_rating = ::ItemRating.find(params[:id])
      if @item_rating.update(create_params)
        render json: {status: true, data: {item_rating: @item_rating}}
      else
        errors = @item_rating.errors.messages.each.map {|k,v| v}
        render json: {status: false, errors: errors.flatten }
      end
    rescue ActiveRecord::RecordNotFound => e
      render json: {status: :false, error: "Item Rating not found",  error_message:e.message} 
    end
  end

  def delete
    begin
      @item_rating = ::ItemRating.find(params[:id])
      if @item_rating.destroy
        render json: {status: true, notice: 'Item Rating is successfully destroyed.'}
      else
        render json: {status: :false, error: "Item Rating details deleted failed."}
      end 
    rescue ActiveRecord::RecordNotFound => e
      render json: {status: false, error: "Item Rating not found",  error_message:e.message} 
    end
  end

  def update_status
    begin
      @item_rating = ::ItemRating.find(params[:id])
      @item_rating.status = params[:status]
      if @item_rating.save!
        flash[:notice] = "Item Rating Status successfully Updated"
      else
        errors = @item_rating.errors.messages.each.map {|k,v| v}
        render json: {status: false, errors: errors.flatten }
      end
    rescue ActiveRecord::RecordNotFound => e
      render json: {status: :false, error: "Item Rating not found",  error_message:e.message} 
    end
  end

  private

  def create_params
    params.require(:item_rating).permit(:item_id, :user_id, :rating, :status)
  end
end
