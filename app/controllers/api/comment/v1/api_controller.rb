class Api::Comment::V1::ApiController < ApplicationController
   respond_to :json
  protect_from_forgery with: :null_session, if: Proc.new { |c| c.request.format =~ %r{application/json} }
  rescue_from ActiveRecord::RecordNotFound, with: :not_found

  private
 
  def authenticate
    #authenticate_or_request_with_http_token do |token, options|
    @current_user = ::User.find_by_auth_token(cookies[:user_auth_token])
   
    unless @current_user
      @current_user = nil
       render html: "<script>alert('Please Login!'); return false;</script>".html_safe
    end
  end
end
