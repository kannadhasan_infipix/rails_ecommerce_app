class Order < ActiveRecord::Base
  belongs_to :user
  belongs_to :item
  enum status: {draft: 0, active: 1, deactive: 2}
  enum payment_type: {credit_card: 0, debit_card: 1, internet_banking: 2, cash_on_delivery:4}
  before_create :assign_order_number

  private
  def assign_order_number
      self.order_id = genreate_order_number
      self.date_of_purchase = Time.now
  end
  def genreate_order_number
    loop do
      new_order_id = SecureRandom.hex(7).upcase
      break new_order_id unless Order.exists?(order_id: new_order_id)
    end
  end
end
