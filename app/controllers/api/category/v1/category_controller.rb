class Api::Category::V1::CategoryController < Api::Category::V1::ApiController
  skip_before_filter :verify_authenticity_token
  before_action :display_menus, :user_account_detail
  def new
    @category = ::Category.new
  end

  def index
    @category = ::Category.all
  end

  def show
    begin
      @category = ::Category.find(params[:id])
      render json: {status: @category }
    rescue ActiveRecord::RecordNotFound => e
      @category = []
    end
  end

  def edit
    begin
      @category = ::Category.find(params[:id])
    rescue ActiveRecord::RecordNotFound => e
      @category = []
    end
  end

  def create
    begin
      category_exist = ::Category.find_by_name(params[:category][:name]) rescue nil
      render json: {status: false, errors: ['Category already exist.']} and return if !category_exist.blank?
      @category = ::Category.new(create_params)
      if @category.save!
        render json: {status: true, data: {Category: @category}}
      else
        errors = @category.errors.messages.each.map {|k,v| v}
        render json: {status: false, errors: errors.flatten }
      end
    rescue Exception => e  
      render json: {status: :false, error: "Category creation failed" ,  error_message:e.message} 
    end
  end

  def update
    begin
      @category = ::Category.find(params[:id])
      if @category.update(create_params)
        render json: {status: true, data: {Category: @category}}
      else
        errors = @category.errors.messages.each.map {|k,v| v}
        render json: {status: false, errors: errors.flatten }
      end
    rescue ActiveRecord::RecordNotFound => e
      render json: {status: :false, error: "Category not found",  error_message:e.message} 
    end
  end

  def delete
    begin
      @category = ::Category.find(params[:id])
      if @category.destroy
        render json: {status: true, notice: 'Category is successfully destroyed.'}
      else
        render json: {status: :false, error: "Category details deleted failed."}
      end 
    rescue ActiveRecord::RecordNotFound => e
      render json: {status: false, error: "Category not found",  error_message:e.message} 
    end
  end

  def update_status
    begin
      @category = ::Category.find(params[:id])
      @category.status = params[:status]
      if @category.save!
        flash[:notice] = "Category Status successfully Updated"
      else
        errors = @category.errors.messages.each.map {|k,v| v}
        render json: {status: false, errors: errors.flatten }
      end
    rescue ActiveRecord::RecordNotFound => e
      render json: {status: :false, error: "Category not found",  error_message:e.message} 
    end
  end

  private

  def create_params
    params.require(:category).permit(:menu_id, :name, :slug, :parent_category, :status)
  end

end
