class Api::Order::V1::OrderController < Api::Order::V1::ApiController
 skip_before_filter :verify_authenticity_token
  before_action :display_menus, :user_account_detail, :authenticate
  def new
    @order = ::Order.new
    @get_order_details = params
    @item_details  = ::Item.select("items.name,item_inventories.price").joins(:item_inventory).where("items.id=?",@get_order_details[:item_id])
    #render json: {status: @item_details}
    @user_details = @current_user
  end

  def index
    @order = ::Item.select("orders.id as o_id, orders.order_id, orders.order_quantity, orders.order_size, orders.order_color, orders.date_of_purchase, orders.payable_amount,orders.status, items.id  ,items.name,items.image,item_inventories.price").joins(:item_inventory).joins(:Order).where("orders.user_id=?",@current_user.id)
    #render json: {status: @order}
  end

  def show
    begin
      @order = ::Order.joins(:User).find(params[:id])
      render json: {status: true, data: {order: @order}}
    rescue ActiveRecord::RecordNotFound => e
      @order = []
       render json: {status: :false, error: "Order not found" ,  error_message:e.message} 
    end
  end

  def edit
    begin
      @order = ::Order.find(params[:id])
    rescue ActiveRecord::RecordNotFound => e
      @order = []
    end
  end

  def create
    begin
      order_exist = ::Order.find_by("user_id=? and item_id=? and vendor_id=?",@current_user.id, params[:order][:item_id] , params[:order][:vendor_id]) rescue nil
      render json: {status: false, errors: ['Your Order already exist.']} and return if !order_exist.blank?
      @order = ::Order.new(create_params_with_user)
      #render json: {status: @order , errors: ['Order already exist.']}
      if @order.save!
        render json: {status: true, data: {order: @order}}
      else
        errors = @order.errors.messages.each.map {|k,v| v}
        render json: {status: false, errors: errors.flatten }
      end
    rescue Exception => e  
      render json: {status: :false, error: "Order creation failed" ,  error_message:e.message} 
    end
  end

  def update
    begin
      @order = ::Order.find(params[:id])
      if @order.update(create_params)
        render json: {status: true, data: {order: @order}}
      else
        errors = @order.errors.messages.each.map {|k,v| v}
        render json: {status: false, errors: errors.flatten }
      end
    rescue ActiveRecord::RecordNotFound => e
      render json: {status: :false, error: "Order not found",  error_message:e.message} 
    end
  end

  def delete
    begin
      @order = ::Order.find(params[:id])
      if @order.destroy
        render json: {status: true, message: 'Order is successfully destroyed.'}
      else
        render json: {status: :false, error: "Order details deleted failed."}
      end 
    rescue ActiveRecord::RecordNotFound => e
      render json: {status: false, error: "Order not found",  error_message:e.message} 
    end
  end

  def update_status
    begin
      @order = ::Order.find(params[:id])
      @order.status = params[:status]
      if @order.save!
        flash[:notice] = "Order Status successfully Updated"
      else
        errors = @order.errors.messages.each.map {|k,v| v}
        render json: {status: false, errors: errors.flatten }
      end
    rescue ActiveRecord::RecordNotFound => e
      render json: {status: :false, error: "Order not found",  error_message:e.message} 
    end
  end

  private

  def create_params_with_user
     create_params.merge({user_id: @current_user.id})
  end

  def create_params
    params.require(:order).permit(:item_id, :vendor_id, :order_quantity,:order_color, :order_size, :payable_amount, :shipping_name, :shipping_address, :shipping_city, :shipping_phone, :shipping_state, :shipping_country, :shipping_pincode, :shipping_landmark, :status)
  end
end
