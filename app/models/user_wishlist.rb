class UserWishlist < ActiveRecord::Base
  belongs_to :user
  belongs_to :item
  enum status: {draft: 0, active: 1, deactive: 2}
end
