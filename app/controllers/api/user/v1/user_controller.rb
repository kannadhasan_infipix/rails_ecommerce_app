class Api::User::V1::UserController < Api::User::V1::ApiController
  skip_before_filter :verify_authenticity_token
  before_action :display_menus, :user_account_detail 
  before_action :authenticate, :except => [:create]
  
  def index
   
  end

  def show
    begin
      @user = ::User.find(@current_user.id)
      @country = ::Country.all
      @state = ::State.where("country_id=?",@user.country_id)
      #render json: {status: @user}
    rescue ActiveRecord::RecordNotFound => e
      @user = []
    end
  end

  def new
    @user = ::User.new
    @country = ::Country.all
  end

  def edit
    begin
      @user = ::User.find(params[:id])
    rescue ActiveRecord::RecordNotFound => e
      @user = []
    end
  end

  def create
    begin
      user_exist = ::User.find_by_email(params[:user][:email]) rescue nil
      render json: {status: false, errors: ['Email already registered.']} and return if !user_exist.blank?
      @user = ::User.new(create_params)

      if @user.save!
        #UserMailer.send_email_verification_token(@user).deliver_later
        redirect_to "/api/user/v1/user/#{@user.id}/show"
        #render json: {status: true, user_auth_token: @user.auth_token}
      else
        errors = @user.errors.messages.each.map {|k,v| v}
        render json: {status: false, errors: errors.flatten }
      end
    rescue Exception => e  
      render json: {status: :false, error: "User creation failed" ,  error_message:e.message} 
    end
  end

  def update
     begin
      @user = ::User.find(params[:id])
      @user.image = params[:image]

      if @user.update(update_params)
        redirect_to "/api/user/v1/user/#{params[:id]}/show"
        #render json: {status: true, data: {user: @user}}
      else
        errors = @user.errors.messages.each.map {|k,v| v}
        render json: {status: false, errors: errors.flatten }
      end
    rescue ActiveRecord::RecordNotFound => e
      render json: {status: :false, error: "User not found",  error_message:e.message} 
    end
  end

  def delete
     begin
      @user = ::User.find(params[:id])
      if @user.destroy
        render json: {status: true, notice: 'User is successfully destroyed.'}
      else
        render json: {status: :false, error: "User details deleted failed."}
      end 
    rescue ActiveRecord::RecordNotFound => e
      render json: {status: false, error: "User not found",  error_message:e.message} 
    end
  end
  
  def login
    render json: {status: false, errors: ['Email or Password is empty.']} and return if login_details_blank?
    user = ::User.find_by_email(params[:email]) rescue nil
    unless user
      render json: {status: false, errors: ['No account exists with this email.']} and return
    end
    if login_authenticate(user)
       cookies[:user_auth_token] =  user.auth_token
       login_user_account(user.id)
       render json: {status: true, messsage:"Login success", useraccount:{cart_count: @cart,cart_amount: @cart_price}}
    else
      render json: {status: false, errors: ['Email and Password does not match.']} and return
    end
  end

  def logout
    cookies.delete(:user_auth_token)
    redirect_to root_path
  end

  def forgot_password
    begin
      @user = ::User.find_by_email(params[:email]) 
      encrypted_password = PasswordEncryptor.new(params[:password]).salted
      @user.salt = encrypted_password.salt
      @user.password = encrypted_password.hash
      
      if @user.save!
        flash[:notice] = "User Password successfully Updated"
        #redirect_to api_user_v1_user_path
      else
        errors = @user.errors.messages.each.map {|k,v| v}
        render json: {status: false, errors: errors.flatten }
      end
    rescue ActiveRecord::RecordNotFound => e
      render json: {status: :false, error: "User not found",  error_message:e.message} 
    end
  end
  def update_image
    begin
      @user = ::User.find(params[:id])
      @user.image = params[:image]
      
      if @user.save!
        flash[:notice] = "User status successfully updated"
      else
        errors = @user.errors.messages.each.map {|k,v| v}
        render json: {status: false, errors: errors.flatten }
      end
    rescue ActiveRecord::RecordNotFound => e
      render json: {status: :false, error: "User not found",  error_message:e.message} 
    end
  end


  def update_status
    begin

      @user = ::User.find(params[:id])
      @user.image = params[:image]
      
      if @user.save!
        flash[:notice] = "User image successfully Updated"
      else
        errors = @user.errors.messages.each.map {|k,v| v}
        render json: {status: false, errors: errors.flatten }
      end
    rescue ActiveRecord::RecordNotFound => e
      render json: {status: :false, error: "User not found",  error_message:e.message} 
    end
  end
  def verify_email
    begin
      @user = ::User.find_by("id=? and email_verification_token=?",params[:id], params[:token])
      @user.email_verified = true
      
      if @user.save!
        render json: {status: true, message: "User Email verified successfully" }
      else
        errors = @user.errors.messages.each.map {|k,v| v}
        render json: {status: false, errors: errors.flatten }
      end
    rescue ActiveRecord::RecordNotFound => e
      render json: {status: :false, error: "User not found",  error_message:e.message} 
    end
  end

  private


  def login_user_account(user_id)
    
    @cart= 0
    @cart_price= 0
    if !user_id_by_auth_token.blank?
     get_count = ::ShoppingCart.select("item_id").where("user_id=?",user_id)
     get_amount = ::ShoppingCart.select("sum(price) as amount").joins(item: :item_inventory).find_by_user_id(user_id)
     @cart = get_count.count 
     if !get_amount.amount.blank?
      @cart_price= get_amount.amount
     end
    end
    #render json: {cart_amount: @cart_price, in_cart: @cart}
  end

  def create_params
    params.permit(:firstname, :lastname, :email, :password, :mobile_number, :image, :address1, :address2, :city, :state_id, :country_id, :pin_code, :status)
  end
  def update_params
    params.permit(:firstname, :lastname, :mobile_number, :image, :address1, :address2, :city, :state_id, :country_id, :pin_code)
  end

  def login_details_blank?
    params[:email].blank? || params[:password].blank?
  end

  def login_authenticate(user)
    return false unless user
    user.password == PasswordEncryptor.new(params[:password], :salt => user.salt).salted.hash
  end
end
