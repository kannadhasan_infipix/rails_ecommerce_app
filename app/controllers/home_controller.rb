class HomeController < ApplicationController
  before_action :display_menus, :user_account_detail
  def index
    @home_latest_item = ::Item.select("items.id, items.name, items.image, item_inventories.price, item_inventories.mrp").joins(:item_inventory).where("items.status=1").limit(6)
    @home_featured_item = ::Item.select("items.id, items.name, items.image, item_inventories.price, item_inventories.mrp").joins(:item_inventory).where("items.status=1").limit(10)
    #render json:{item: @home_latest_item}
  end
end
