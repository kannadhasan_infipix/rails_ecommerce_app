class Administrator < ActiveRecord::Base

  enum status: {draft: 0, active: 1, deactive: 2}

  before_create :assign_salt_and_hashed_password
  before_create :assign_auth_token
  
  private
  
  def assign_auth_token
    self.auth_token = generate_auth_token
  end

  def assign_salt_and_hashed_password
    encrypted_password = PasswordEncryptor.new(self.password).salted
    self.salt = encrypted_password.salt
    self.password = encrypted_password.hash
  end

  def generate_auth_token
    loop do
      token = SecureRandom.base64.tr('+/=', self.email)
      break token unless Administrator.exists?(auth_token: token)
    end
  end
end
