class VendorMailer < ApplicationMailer
  def send_email_verification_token(vendor)
    @vendor = vendor
    mail(to: @vendor.email, subject: 'Online shopping - Email verification')
  end
end
