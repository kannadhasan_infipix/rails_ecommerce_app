class Api::Search::V1::SearchController < Api::Search::V1::ApiController
  before_action :display_menus, :user_account_detail
  def search_by_category
    @item = ::Item.select("items.id, items.name, items.image, item_inventories.price, item_inventories.mrp").joins(:item_inventory).where("items.category_id=?",params[:id])
    #render json: {status: @item}
  end
  def search_by_category_name
    category_id = ::Category.find_by_name(params[:category_name])
    @item = ::Item.select("items.id, items.name, items.image, item_inventories.price, item_inventories.mrp").joins(:item_inventory).where("items.category_id=?",category_id.id)
    #render json: {status: category_id}
  end
  def search_brand_name
    @brand = ::Brand.select("id,name").where("name LIKE :query", query: "%#{params[:query]}%").limit(10)
    render json: {status: true, data: @brand}
  end

  def search_menu_name
    @menu = ::Menu.select("id,name").where("name LIKE :query", query: "%#{params[:query]}%").limit(10)
    render json: {status: true, data: @menu}
  end
  def search_category_name
    @category = ::Category.select("id,name").where("menu_id=?", params[:menu_id]).where("name LIKE :query", query: "%#{params[:query]}%").limit(10)
    render json: {status: true, data: @category}
  end
  def search_item_name
    @item = ::Item.select("id,name").where("items.name LIKE :query", query: "%#{params[:query]}%").limit(10)
    render json: {status: true, data: @item}
  end

   def search_store_name
    @store = ::Store.select("id, name").where("stores.vendor_id=?",vendor_info.id).where("stores.name LIKE :query", query: "%#{params[:query]}%").limit(10)
    render json: {status: true, data: @store}
  end

  def search_item_by_id
    @item = ::Item.select("items.*,brands.id as brand_id,brands.name as brand_name, menus.id as menu_id,menus.name as menu_name, categories.id as cat_id,categories.name as cate_name").joins(:brand).joins(:menu).joins(:category).where("items.id=?", params[:id]).limit(10)
    render json: {status: true, data: @item}
  end

  private
  def vendor_info

    @current_user = ::Vendor.find_by_auth_token(cookies[:vendor_auth_token])
    
  end
end
