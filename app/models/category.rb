class Category < ActiveRecord::Base
  belongs_to :menu
  #has_many :item
  enum status: {draft: 0, active: 1, deactive: 2}
end
