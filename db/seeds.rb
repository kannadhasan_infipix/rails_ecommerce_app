
print "......."
print "Creating Administrator ********  "
Administrator.create([
  { profile_name: "admin", email: "admin@infipix.com", password: "admin@123", status: "active" },
  { profile_name: "admin", email: "kannadhasan@infipix.com", password: "kannadhasan@infipix", status: "active" }
  ])
print "Created Administrator "
puts "  ================ "

print "......."
print "Creating Menu ********  "
Menu.create([
  {name: "Men", slug:"men", status: "active"},
  {name: "Women", slug:"women", status: "active"}

  ])
print "Created Menu "
puts "  ================ "

print "......."
print "Creating Category ********  "
Category.create([
  {menu_id: 1, name: "Footwear", slug: "men-footwear", status: "active"},
  {menu_id: 1, name: "Topwear", slug: "men-topwear", status: "active"},
  {menu_id: 1, name: "Bottomwear", slug: "men-bottomwear", status: "active"},
  {menu_id: 1, name: "Sportswear", slug: "men-sportswear", status: "active"},
  {menu_id: 1, name: "Accessories", slug: "men-accessories", status: "active"}
  ])
print "Created Category"
puts "  ================ "

print "......."
print "Creating Sub Category ********  "
Category.create([
  {menu_id: 1, name: "Sports Shoes", slug:"men-sport-shoes", parent_category:1, status: "active"},
  {menu_id: 1, name: "Casual Shoes",  slug:"men-casual-shoes", parent_category:1, status: "active"},
  {menu_id: 1, name: "Formal Shoes", slug:"men-formal-shoes", parent_category:1, status: "active"},
  {menu_id: 1, name: "Sandal Shoes", slug:"men-sandal-shoes", parent_category:1, status: "active"},
  {menu_id: 1, name: "Flip Flop Shoes", slug:"men-flip-flop-shoes", parent_category:1, status: "active"},
  {menu_id: 1, name: "Loafers Shoes", slug:"men-loafers-shoes", parent_category:1, status: "active"},
  {menu_id: 1, name: "Boots", slug:"men-boots", parent_category:1, status: "active"},
  {menu_id: 1, name: "Running Shoes", slug:"men-running-shoes", parent_category:1, status: "active"},
  {menu_id: 1, name: "Snakers Shoes", slug:"men-snakers-shoes", parent_category:1, status: "active"}
  ])
print "Created Sub Category"
puts "  ================ "

print "......."
print "Creating Sub Category ********  "
Category.create([
  {menu_id: 1, name: "T-Shirts", slug:"men-tshirts",parent_category:2, status: "active"},
  {menu_id: 1, name: "Shirts", slug:"men-shirts",parent_category:2, status: "active"},
  {menu_id: 1, name: "Kurtas", slug:"men-Kurtas",parent_category:2, status: "active"},
  {menu_id: 1, name: "Suits & Blazers", slug:"men-suit&blazers",parent_category:2, status: "active"},
  {menu_id: 1, name: "Jackets", slug:"men-jackets",parent_category:2, status: "active"},
  {menu_id: 1, name: "Sweet Shirts", slug:"men-sweet-shirts",parent_category:2, status: "active"}
  ])
print "Created Sub Category"
puts "  ================ "

print "......."
print "Creating Sub Category ********  "
Category.create([
  {menu_id: 1, name: "Jeans", slug:"men-jeans", parent_category:3, status: "active"},
  {menu_id: 1, name: "Trousers", slug:"men-trousers", parent_category:3, status: "active"},
  {menu_id: 1, name: "Shorts", slug:"men-shorts", parent_category:3, status: "active"},
  {menu_id: 1, name: "Cargos", slug:"men-cargos", parent_category:3, status: "active"},
  {menu_id: 1, name: "Track Pants", slug:"men-track-pants", parent_category:3, status: "active"}
  ])
print "Created Sub Category"
puts "  ================ "

print "......."
print "Creating Sub Category ********  "
Category.create([
  {menu_id: 1, name: "Sports T-Shirts", slug:"men-sports-tshirts",  parent_category:4, status: "active"},
  {menu_id: 1, name: "Track Pants", slug:"men-sport-track-pants",  parent_category:4, status: "active"},
  {menu_id: 1, name: "Track Suits", slug:"men-sport-track-suits",  parent_category:4, status: "active"},
  {menu_id: 1, name: "Shorts", slug:"men-sports-shorts", parent_category:4, status: "active"}
  ])
print "Created Sub Category"
puts "  ================ "

print "......."
print "Creating Sub Category ********  "
Category.create([
  {menu_id: 1, name: "Watches", slug:"men-watches", parent_category:5, status: "active"},
  {menu_id: 1, name: "Belts", slug:"men-belts", parent_category:5, status: "active"},
  {menu_id: 1, name: "Bags & Wallets", slug:"men-bags-wallets", parent_category:5, status: "active"},
  {menu_id: 1, name: "Shavers", slug:"men-shavers", parent_category:5, status: "active"},
  {menu_id: 1, name: "trimmers", slug:"men-trimmers", parent_category:5, status: "active"}
  ])
print "Created Sub Category"
puts "  ================ "




print "......."
print "Creating Category ********  "
Category.create([
  {menu_id: 2, name: "Footwear", slug:"women-footwear", status: "active"},
  {menu_id: 2, name: "Jewellery", slug:"women-jewellery", status: "active"},
  {menu_id: 2, name: "Ethnicwear", slug:"women-ethnic", status: "active"},
  {menu_id: 2, name: "Westernwear", slug:"women-western", status: "active"},
  {menu_id: 2, name: "Lingerie & Sleepwear", slug:"women-lingeric-sleeper", status: "active"},
  {menu_id: 2, name: "Accessories", slug:"women-accessories", status: "active"}
  ])
print "Created Category"
puts "  ================ "

print "......."
print "Creating Sub Category ********  "
Category.create([
  {menu_id: 2, name: "Flats", slug:"women-flat-footwear", parent_category:35, status: "active"},
  {menu_id: 2, name: "Heels", slug:"women-heels", parent_category:35, status: "active"},
  {menu_id: 2, name: "Wedges", slug:"women-wedges", parent_category:35, status: "active"},
  {menu_id: 2, name: "Sports Shoes", slug:"women-sports-shoes", parent_category:35, status: "active"},
  {menu_id: 2, name: "Slippers & Flip Flop Shoes", slug:"women-slipper-flip-flop", parent_category:35, status: "active"},
  {menu_id: 2, name: "Boots", slug:"women-boots", parent_category:35, status: "active"},
  {menu_id: 2, name: "Ballerinas", slug:"women-ballerinas", parent_category:35, status: "active"},
  {menu_id: 2, name: "Casual Shoes", slug:"women-casual-shoes", parent_category:35, status: "active"}
  ])
print "Created Sub Category"
puts "  ================ "

print "......."
print "Creating Sub Category ********  "
Category.create([
  {menu_id: 2, name: "Sarees", slug:"women-sarees", parent_category:36, status: "active"},
  {menu_id: 2, name: "Kurtas & Kurits", slug:"women-kurtas-kurtis", parent_category:36, status: "active"},
  {menu_id: 2, name: "Dress Material", slug:"women-dress-material", parent_category:36, status: "active"},
  {menu_id: 2, name: "Lehangas Choli", slug:"women-lehangas", parent_category:36, status: "active"},
  {menu_id: 2, name: "Blouse", slug:"women-blouse", parent_category:36, status: "active"},
  {menu_id: 2, name: "Leggings & Salwars", slug:"women-leggings-salwars", parent_category:36, status: "active"}
  ])
print "Created Sub Category"
puts "  ================ "

print "......."
print "Creating Sub Category ********  "
Category.create([
  {menu_id: 2, name: "Tops, T-Shirts & Shirts", slug:"women-tops-t-shirts", parent_category:37, status: "active"},
  {menu_id: 2, name: "Jeans", slug:"women-jeans", parent_category:37, status: "active"},
  {menu_id: 2, name: "Shorts & Shirts", slug:"women-shorts-shirts", parent_category:37, status: "active"},
  {menu_id: 2, name: "Trousers & Capris", slug:"women-trousers-capris", parent_category:37, status: "active"}
  ])
print "Created Sub Category"
puts "  ================ "

print "......."
print "Creating Sub Category ********  "
Category.create([
  {menu_id: 2, name: "Bras", slug:"women-bras", parent_category:38, status: "active"},
  {menu_id: 2, name: "Panties", slug:"women-panties", parent_category:38, status: "active"},
  {menu_id: 2, name: "Nightsuits & Nightdresses", slug:"women-nightsdress", parent_category:38, status: "active"}
  ])
print "Created Sub Category"
puts "  ================ "

print "......."
print "Creating Sub Category ********  "
Category.create([
  {menu_id: 2, name: "Bands", slug:"women-bands", parent_category:39, status: "active"},
  {menu_id: 2, name: "Handbags", slug:"women-hanbags", parent_category:39, status: "active"},
  {menu_id: 2, name: "Belts & Wallets", slug:"women-belts-wallets", parent_category:39, status: "active"},
  {menu_id: 2, name: "Frames", slug:"women-frames", parent_category:39, status: "active"},
  {menu_id: 2, name: "Hair Straightners", slug:"women-hair-strightners", parent_category:39, status: "active"},
  {menu_id: 2, name: "Hair Dryers", slug:"women-hair-dryers", parent_category:39, status: "active"}
  ])
print "Created Sub Category"
puts "  ================ "


print "......."
print "Creating Brands ********  "
Brand.create([
  { name: "Ray Ban", slug:"brand-ray-ban", description:"This is new brand for Shopping", status: "active"},
  { name: "7 trendz", slug:"brand-7-trendz", description:"This is new brand for Shopping", status: "active"},
  { name: "99 T-Shirts", slug:"brand-99-T-Shirts", description:"This is new brand for Shopping", status: "active"},
  { name: "Adidas", slug:"brands-adidas", description:"This is new brand for Shopping", status: "active"},
  { name: "American indico", slug:"brands-allen-jolly", description:"This is new brand for Shopping", status: "active"},
  { name: "Black Boy", slug:"brands-balck-boy", description:"This is new brand for Shopping", status: "active"},
  { name: "Citizen", slug:"brands-citizen", description:"This is new brand for Shopping", status: "active"},
  { name: "Ramarajan", slug:"brands-ramarajan", description:"This is new brand for Shopping", status: "active"},
  { name: "Puma", slug:"brands-puma", description:"This is new brand for Shopping", status: "active"},
  { name: "Jockey", slug:"brands-jockey", description:"This is new brand for Shopping", status: "active"},
  { name: "Arrow", slug:"Arrow", description:"This is new brand for Shopping", status: "active"}
  ])
print "Created Brands"
puts "  ================ "


print "......."
print "Creating Country ********  "
Country.create([
  {name: "India"},
  {name: "Russia"},
  {name: "Dubai"},
  {name: "London"},
  {name: "America"},
  {name: "China"},
  {name: "Jappan"},
  {name: "Canada"}

  ])
print "Created Country "
puts "  ================ "



print "......."
print "Creating State ********  "
State.create([
  {name: "Tamil Nadu", country_id: 1},
  {name: "Kerala", country_id: 1},
  {name: "Karnataka", country_id: 1},
  {name: "Telungana", country_id: 1},
  {name: "Delhi", country_id: 1},
  {name: "MAharastra", country_id: 1},
  {name: "Rajasthan", country_id: 1},
  {name: "Kolkatta", country_id: 1}

  ])
print "Created State "
puts "  ================ "


print "......."
print "Creating Vendor ********  "
Vendor.create([
  { firstname:"admin", lastname:"super", email:"admin@shopping.com", password:"12345", mobile_number:"1234567890", image:"", address1:"2nd Street", address2:"Ram nagar, Madipakkam", city:"Chennai", pin_code:"600091", status:"active", state_id:"1", country_id:"1" },
  { firstname:"raja", lastname:"sekar", email:"raja@shopping.com", password:"12345", mobile_number:"1234567890", image:"", address1:"2nd Street", address2:"Ram nagar, velachery", city:"Chennai", pin_code:"600092", status:"active", state_id:"1", country_id:"1" },
  { firstname:"deva", lastname:"indiaran", email:"deva@shopping.com", password:"12345", mobile_number:"1234567890", image:"", address1:"2nd Street", address2:"Ram nagar, medavakkam", city:"Chennai", pin_code:"600093", status:"active", state_id:"1", country_id:"1" },
  { firstname:"saran", lastname:"raj", email:"saran@shopping.com", password:"12345", mobile_number:"1234567890", image:"", address1:"2nd Street", address2:"Ram nagar, pallikaranai", city:"Chennai", pin_code:"600094", status:"active", state_id:"1", country_id:"1" },
  { firstname:"kumar", lastname:"raj", email:"kumar@shopping.com", password:"12345", mobile_number:"1234567890", image:"", address1:"2nd Street", address2:"Ram nagar, Tharamani", city:"Chennai", pin_code:"600095", status:"active", state_id:"1", country_id:"1" }
  ])
print "Created Vendor "
puts "  ================ "

print "......."
print "Creating Vendor Account ********  "
VendorAccount.create([
  {vendor_id:"1", acc_number:"123123", acc_name:"admin", bank_name:"indian bank", bank_branch_name:"chennai", branch_address:"chennai", ifsc_code:"12345", acc_type:"savings", status:"active" },
  {vendor_id:"2", acc_number:"234234", acc_name:"raja", bank_name:"indian bank", bank_branch_name:"chennai", branch_address:"chennai", ifsc_code:"12345", acc_type:"savings", status:"active" },
  {vendor_id:"3", acc_number:"345456", acc_name:"deva", bank_name:"indian bank", bank_branch_name:"chennai", branch_address:"chennai", ifsc_code:"12345", acc_type:"savings", status:"active" },
  {vendor_id:"4", acc_number:"567576", acc_name:"saran", bank_name:"indian bank", bank_branch_name:"chennai", branch_address:"chennai", ifsc_code:"12345", acc_type:"savings", status:"active" },
  {vendor_id:"5", acc_number:"678687", acc_name:"kumar", bank_name:"indian bank", bank_branch_name:"chennai", branch_address:"chennai", ifsc_code:"12345", acc_type:"savings", status:"active" }
  ])
print "Created Vendor Account "
puts "  ================ "

print "......."
print "Creating Store ********  "
Store.create([
  { vendor_id:1, name: "Saravana Store", slug:"store-saravana-store", description:"This is new Store for Shopping", status: "active"},
  { vendor_id:1, name: "Reliance Trendz", slug:"store-reliance-trendz", description:"This is new Store for Shopping", status: "active"},
  { vendor_id:1, name: "Big Bazzar", slug:"store-bigbazzer", description:"This is new Store for Shopping", status: "active"},
  { vendor_id:1, name: "Adidas", slug:"store-adidas", description:"This is new Store for Shopping", status: "active"},
  { vendor_id:1, name: "adict", slug:"store-adict", description:"This is new Store for Shopping", status: "active"}

  ])
print "Created Store"
puts "  ================ "


print "......."
print "Creating User ********  "
User.create([
  { firstname:"admin", lastname:"super", email:"admin@shopping.com", password:"12345", mobile_number:"1234567890", image:"", address1:"2nd Street", address2:"Ram nagar, Madipakkam", city:"Chennai", pin_code:"600091", status:"active", state_id:"1", country_id:"1" },
  { firstname:"raja", lastname:"sekar", email:"raja@shopping.com", password:"12345", mobile_number:"1234567890", image:"", address1:"2nd Street", address2:"Ram nagar, velachery", city:"Chennai", pin_code:"600092", status:"active", state_id:"1", country_id:"1" },
  { firstname:"deva", lastname:"indiaran", email:"deva@shopping.com", password:"12345", mobile_number:"1234567890", image:"", address1:"2nd Street", address2:"Ram nagar, medavakkam", city:"Chennai", pin_code:"600093", status:"active", state_id:"1", country_id:"1" },
  { firstname:"saran", lastname:"raj", email:"saran@shopping.com", password:"12345", mobile_number:"1234567890", image:"", address1:"2nd Street", address2:"Ram nagar, pallikaranai", city:"Chennai", pin_code:"600094", status:"active", state_id:"1", country_id:"1" },
  { firstname:"kumar", lastname:"raj", email:"kumar@shopping.com", password:"12345", mobile_number:"1234567890", image:"", address1:"2nd Street", address2:"Ram nagar, Tharamani", city:"Chennai", pin_code:"600095", status:"active", state_id:"1", country_id:"1" }
  ])
print "Created Vendor "
puts "  ================ "

print "......."
print "Creating Vendor Account ********  "
UserAccount.create([
  {user_id:"1", acc_number:"123123", acc_name:"admin", bank_name:"indian bank", bank_branch_name:"chennai", branch_address:"chennai", ifsc_code:"12345", acc_type:"savings", status:"active" },
  {user_id:"2", acc_number:"234234", acc_name:"raja", bank_name:"indian bank", bank_branch_name:"chennai", branch_address:"chennai", ifsc_code:"12345", acc_type:"savings", status:"active" },
  {user_id:"3", acc_number:"345456", acc_name:"deva", bank_name:"indian bank", bank_branch_name:"chennai", branch_address:"chennai", ifsc_code:"12345", acc_type:"savings", status:"active" },
  {user_id:"4", acc_number:"567576", acc_name:"saran", bank_name:"indian bank", bank_branch_name:"chennai", branch_address:"chennai", ifsc_code:"12345", acc_type:"savings", status:"active" },
  {user_id:"5", acc_number:"678687", acc_name:"kumar", bank_name:"indian bank", bank_branch_name:"chennai", branch_address:"chennai", ifsc_code:"12345", acc_type:"savings", status:"active" }
  ])
print "Created Vendor Account "
puts "  ================ "

print "......."
print "Creating Item ********  "
Item.create([
  {
    name:"Being Fab Mens Solid Formal Blue Shirt",
    heading:"Special PriceExtra 15% Off on Fashion & Lifestyle",
    sub_heading:"Special PriceFlat ₹500 Off",
    image:"p1.jpg",
    brand_id:"1",
    menu_id:"1",
    category_id:"2",
    description:"Henley neck full sleeve t-shirt with robe on the collar and with attractive color ",
    features:
    {
      "made_in":"India",
      "item_size": 
      {
        "38":"XS",
        "39":"S",
        "40":"M",
        "42":"L",
        "44":"XL",
        "46":"XXL"
      },
      "color": "Red,White,Black,Green,Sandle",
      "specifications": 
      {
        "Fit":"Regular",
        "Fabric":"Cotton",
        "Sleeve":"Full Sleeve",
        "Pattern":"Checkered",
        "Pack_of":"1",
        "Style_Code":"1376239",
        "Reversible":"No",
        "Fabric_Care":"Machine-wash",
        "Suitable_For":"Western Wear"
      }

    },
    status:"active"
    }
  ])
print "Created Item "
puts "  ================ "

print "......."
print "Creating Item ********  "
Item.create([
  {
  name:"Rodid Solid Mens Henley Blue, Grey T-Shirt",
  heading:"Special PriceExtra 15% Off on Fashion & Lifestyle",
  sub_heading:"Special PriceFlat ₹500 Off",
  image:"p2.jpg",
  brand_id:"1",
  menu_id:"1",
  category_id:"2",
  description:"Henley neck full sleeve t-shirt with robe on the collar and with attractive color combination Navy blue body and Charcoal Melange sleeves, collar and button placket gives a trendy look. The cotton fabric keeps you comfortable throughout the day. We recommend you to team this piece with a pair of denims or chinos and casual shoes to get a cool look.",
  features:
  {
    "made_in":"India",
    "item_size":
    {
      "38":"XS",
      "39":"S",
      "40":"M",
      "42":"L",
      "44":"XL",
      "46":"XXL"
    } ,
    "color": "Red,White,Black,Green,Sandle",
    "specifications":
    {
      "Fit":"Regular",
      "Fabric":"Cotton",
      "Sleeve":"Full Sleeve",
      "Pattern":"Checkered",
      "Pack_of":"1",
      "Style_Code":"1376239",
      "Reversible":"No",
      "Fabric_Care":"Machine-wash",
      "Suitable_For":"Western Wear"
    }

  },
  status:"active"
}
  ])
print "Created Item "
puts "  ================ "

print "......."
print "Creating Item ********  "
Item.create([
  {
    name:"Suspense Mens Solid Casual Dark Blue Shirt",
    heading:"Special PriceExtra 15% Off on Fashion & Lifestyle",
    sub_heading:"Special PriceFlat ₹500 Off",
    image:"p3.jpg",
    brand_id:"1",
    menu_id:"1",
    category_id:"2",
    description:"Henley neck full sleeve t-shirt with robe on the collar and with attractive color combination Navy blue body and Charcoal Melange sleeves, collar and button placket gives a trendy look. The cotton fabric keeps you comfortable throughout the day. We recommend you to team this piece with a pair of denims or chinos and casual shoes to get a cool look.",
    features:
    {
      "made_in":"India",
      "item_size": 
      {
        "38":"XS",
        "39":"S",
        "40":"M",
        "42":"L",
        "44":"XL",
        "46":"XXL"
      },
      "color": "Red,White,Black,Green,Sandle",
      "specifications":
      {
        "Fit":"Regular",
        "Fabric":"Cotton",
        "Sleeve":"Full Sleeve",
        "Pattern":"Checkered",
        "Pack_of":"1",
        "Style_Code":"1376239",
        "Reversible":"No",
        "Fabric_Care":"Machine-wash",
        "Suitable_For":"Western Wear"
      }

    },
    status:"active"
  }
  ])
print "Created Item "
puts "  ================ "
print "......."
print "Creating Item ********  "
Item.create([
  {
    name:"Deeksha Mens Solid Casual Purple Shirt",
    heading:"Special PriceExtra 15% Off on Fashion & Lifestyle",
    sub_heading:"Special PriceFlat ₹500 Off",
    image:"p4.jpg",
    brand_id:"1",
    menu_id:"1",
    category_id:"2",
    description:"Henley neck full sleeve t-shirt with robe on the collar and with attractive color combination Navy blue body and Charcoal Melange sleeves, collar and button placket gives a trendy look. The cotton fabric keeps you comfortable throughout the day. We recommend you to team this piece with a pair of denims or chinos and casual shoes to get a cool look.",
    features:
    {
    "made_in":"India",
    "item_size": 
    {
    "38":"XS",
    "39":"S",
    "40":"M",
    "42":"L",
    "44":"XL",
    "46":"XXL"
    },
    "color": "Red,White,Black,Green,Sandle",
    "specifications": 
    {
    "Fit":"Regular",
    "Fabric":"Cotton",
    "Sleeve":"Full Sleeve",
    "Pattern":"Checkered",
    "Pack_of":"1",
    "Style_Code":"1376239",
    "Reversible":"No",
    "Fabric_Care":"Machine-wash",
    "Suitable_For":"Western Wear"
    }

    },
    status:"active"
    }
  ])
print "Created Item "
puts "  ================ "
print "......."
print "Creating Item ********  "
Item.create([
  {
    name:"Being Fab Mens Solid Formal Blue Shirt",
    heading:"Special PriceExtra 15% Off on Fashion & Lifestyle",
    sub_heading:"Special PriceFlat ₹500 Off",
    image:"p5.jpg",
    brand_id:"1",
    menu_id:"1",
    category_id:"2",
    description:"Henley neck full sleeve t-shirt with robe on the collar and with attractive color combination Navy blue body and Charcoal Melange sleeves, collar and button placket gives a trendy look. The cotton fabric keeps you comfortable throughout the day. We recommend you to team this piece with a pair of denims or chinos and casual shoes to get a cool look.",
    features:{
    "made_in":"India",
    "item_size": 
    {
    "38":"XS",
    "39":"S",
    "40":"M",
    "42":"L",
    "44":"XL",
    "46":"XXL"
    },
    "color": "Red,White,Black,Green,Sandle",
    "specifications": 
    {
    "Fit":"Regular",
    "Fabric":"Cotton",
    "Sleeve":"Full Sleeve",
    "Pattern":"Checkered",
    "Pack_of":"1",
    "Style_Code":"1376239",
    "Reversible":"No",
    "Fabric_Care":"Machine-wash",
    "Suitable_For":"Western Wear"
    }

    },
    status:"active"
    }
  ])
print "Created Item "
puts "  ================ "
print "......."
print "Creating Item ********  "
Item.create([
  {
    name:"Rodid Solid Mens Henley Blue, Grey T-Shirt",
    heading:"Special PriceExtra 15% Off on Fashion & Lifestyle",
    sub_heading:"Special PriceFlat ₹500 Off",
    image:"p6.jpg",
    brand_id:"1",
    menu_id:"1",
    category_id:"2",
    description:"Henley neck full sleeve t-shirt with robe on the collar and with attractive color combination Navy blue body and Charcoal Melange sleeves, collar and button placket gives a trendy look. The cotton fabric keeps you comfortable throughout the day. We recommend you to team this piece with a pair of denims or chinos and casual shoes to get a cool look.",
    features:
    {
    "made_in":"India",
    "item_size": 
    {
    "38":"XS",
    "39":"S",
    "40":"M",
    "42":"L",
    "44":"XL",
    "46":"XXL"
    },
    "color": "Red,White,Black,Green,Sandle",
    "specifications":
    {
    "Fit":"Regular",
    "Fabric":"Cotton",
    "Sleeve":"Full Sleeve",
    "Pattern":"Checkered",
    "Pack_of":"1",
    "Style_Code":"1376239",
    "Reversible":"No",
    "Fabric_Care":"Machine-wash",
    "Suitable_For":"Western Wear"
    }

    },
    status:"active"
    }
  ])
print "Created Item "
puts "  ================ "
print "......."
print "Creating Item ********  "
Item.create([
  {
    name:"Being Fab Mens Solid Formal Blue Shirt",
    heading:"Special PriceExtra 15% Off on Fashion & Lifestyle",
    sub_heading:"Special PriceFlat ₹500 Off",
    image:"p7.jpg",
    brand_id:"1",
    menu_id:"1",
    category_id:"2",
    description:"Henley neck full sleeve t-shirt with robe on the collar and with attractive color combination Navy blue body and Charcoal Melange sleeves, collar and button placket gives a trendy look. The cotton fabric keeps you comfortable throughout the day. We recommend you to team this piece with a pair of denims or chinos and casual shoes to get a cool look.",
    features:
    {
    "made_in":"India",
    "item_size": 
    {
    "38":"XS",
    "39":"S",
    "40":"M",
    "42":"L",
    "44":"XL",
    "46":"XXL"
    },
    "color": "Red,White,Black,Green,Sandle",
    "specifications": 
    {
    "Fit":"Regular",
    "Fabric":"Cotton",
    "Sleeve":"Full Sleeve",
    "Pattern":"Checkered",
    "Pack_of":"1",
    "Style_Code":"1376239",
    "Reversible":"No",
    "Fabric_Care":"Machine-wash",
    "Suitable_For":"Western Wear"
    }

    },
    status:"active"
    }
  ])
print "Created Item "
puts "  ================ "
print "......."
print "Creating Item ********  "
Item.create([
  {
    name:"Rodid Solid Mens Henley Blue, Grey T-Shirt",
    heading:"Special PriceExtra 15% Off on Fashion & Lifestyle",
    sub_heading:"Special PriceFlat ₹500 Off",
    image:"p8.jpg",
    brand_id:"1",
    menu_id:"1",
    category_id:"2",
    description:"Henley neck full sleeve t-shirt with robe on the collar and with attractive color combination Navy blue body and Charcoal Melange sleeves, collar and button placket gives a trendy look. The cotton fabric keeps you comfortable throughout the day. We recommend you to team this piece with a pair of denims or chinos and casual shoes to get a cool look.",
    features:
    {
    "made_in":"India",
    "item_size":
    {
    "38":"XS",
    "39":"S",
    "40":"M",
    "42":"L",
    "44":"XL",
    "46":"XXL"
    },
    "color": "Red,White,Black,Green,Sandle",
    "specifications": 
    {
    "Fit":"Regular",
    "Fabric":"Cotton",
    "Sleeve":"Full Sleeve",
    "Pattern":"Checkered",
    "Pack_of":"1",
    "Style_Code":"1376239",
    "Reversible":"No",
    "Fabric_Care":"Machine-wash",
    "Suitable_For":"Western Wear"
    }

    },
    status:"active"
    }
  ])
print "Created Item "
puts "  ================ "
print "......."
print "Creating Item ********  "
Item.create([
  {
    name:"Being Fab Mens Solid Formal Blue Shirt",
    heading:"Special PriceExtra 15% Off on Fashion & Lifestyle",
    sub_heading:"Special PriceFlat ₹500 Off",
    image:"p9.jpg",
    brand_id:"1",
    menu_id:"1",
    category_id:"2",
    description:"Henley neck full sleeve t-shirt with robe on the collar and with attractive color combination Navy blue body and Charcoal Melange sleeves, collar and button placket gives a trendy look. The cotton fabric keeps you comfortable throughout the day. We recommend you to team this piece with a pair of denims or chinos and casual shoes to get a cool look.",
    features:
    {
    "made_in":"India",
    "item_size":
    {
    "38":"XS",
    "39":"S",
    "40":"M",
    "42":"L",
    "44":"XL",
    "46":"XXL"
    },
    "color": "Red,White,Black,Green,Sandle",
    "specifications": 
    {
    "Fit":"Regular",
    "Fabric":"Cotton",
    "Sleeve":"Full Sleeve",
    "Pattern":"Checkered",
    "Pack_of":"1",
    "Style_Code":"1376239",
    "Reversible":"No",
    "Fabric_Care":"Machine-wash",
    "Suitable_For":"Western Wear"
    }

    },
    status:"active"
    }
  ])
print "Created Item "
puts "  ================ "
print "......."
print "Creating Item ********  "
Item.create([
  {
    name:"Being Fab Mens Solid Formal Blue Shirt",
    heading:"Special PriceExtra 15% Off on Fashion & Lifestyle",
    sub_heading:"Special PriceFlat ₹500 Off",
    image:"l1.jpg",
    brand_id:"1",
    menu_id:"1",
    category_id:"2",
    description:"Henley neck full sleeve t-shirt with robe on the collar and with attractive color combination Navy blue body and Charcoal Melange sleeves, collar and button placket gives a trendy look. The cotton fabric keeps you comfortable throughout the day. We recommend you to team this piece with a pair of denims or chinos and casual shoes to get a cool look.",
    features:
    {
    "made_in":"India",
    "item_size": 
    {
    "38":"XS",
    "39":"S",
    "40":"M",
    "42":"L",
    "44":"XL",
    "46":"XXL"
    },
    "color": "Red,White,Black,Green,Sandle",
    "specifications": 
    {
    "Fit":"Regular",
    "Fabric":"Cotton",
    "Sleeve":"Full Sleeve",
    "Pattern":"Checkered",
    "Pack_of":"1",
    "Style_Code":"1376239",
    "Reversible":"No",
    "Fabric_Care":"Machine-wash",
    "Suitable_For":"Western Wear"
    }

    },
    status:"active"
    }
  ])
print "Created Item "
puts "  ================ "
print "......."
print "Creating Item ********  "
Item.create([
  {
    name:"Rodid Solid Mens Henley Blue, Grey T-Shirt",
    heading:"Special PriceExtra 15% Off on Fashion & Lifestyle",
    sub_heading:"Special PriceFlat ₹500 Off",
    image:"l2.jpg",
    brand_id:"1",
    menu_id:"1",
    category_id:"2",
    description:"Henley neck full sleeve t-shirt with robe on the collar and with attractive color combination Navy blue body and Charcoal Melange sleeves, collar and button placket gives a trendy look. The cotton fabric keeps you comfortable throughout the day. We recommend you to team this piece with a pair of denims or chinos and casual shoes to get a cool look.",
    features:
    {
    "made_in":"India",
    "item_size":
    {
    "38":"XS",
    "39":"S",
    "40":"M",
    "42":"L",
    "44":"XL",
    "46":"XXL"
    },
    "color": "Red,White,Black,Green,Sandle",
    "specifications": 
    {
    "Fit":"Regular",
    "Fabric":"Cotton",
    "Sleeve":"Full Sleeve",
    "Pattern":"Checkered",
    "Pack_of":"1",
    "Style_Code":"1376239",
    "Reversible":"No",
    "Fabric_Care":"Machine-wash",
    "Suitable_For":"Western Wear"
    }

    },
    status:"active"
    }
  ])
print "Created Item "
puts "  ================ "
print "......."
print "Creating Item ********  "
Item.create([
  {
    name:"John Players Mens Formal Shirt",
    heading:"Special PriceExtra 15% Off on Fashion & Lifestyle",
    sub_heading:"Special PriceFlat ₹500 Off",
    image:"l3.jpg",
    brand_id:"1",
    menu_id:"1",
    category_id:"2",
    description:"Henley neck full sleeve t-shirt with robe on the collar and with attractive color combination Navy blue body and Charcoal Melange sleeves, collar and button placket gives a trendy look. The cotton fabric keeps you comfortable throughout the day. We recommend you to team this piece with a pair of denims or chinos and casual shoes to get a cool look.",
    features:
    {
    "made_in":"India",
    "item_size":
    {
    "38":"XS",
    "39":"S",
    "40":"M",
    "42":"L",
    "44":"XL",
    "46":"XXL"
    },
    "color": "Red,White,Black,Green,Sandle",
    "specifications": 
    {
    "Fit":"Regular",
    "Fabric":"Cotton",
    "Sleeve":"Full Sleeve",
    "Pattern":"Checkered",
    "Pack_of":"1",
    "Style_Code":"1376239",
    "Reversible":"No",
    "Fabric_Care":"Machine-wash",
    "Suitable_For":"Western Wear"
    }

    },
    status:"active"
    }
  ])
print "Created Item "
puts "  ================ "


print "......."
print "Creating ItemInventory ********  "
ItemInventory.create([
  { item_id:"1", vendor_id:"1", store_id:"1", price:"555", mrp:"676", instock:"5", discount:"34", status:"active" },
  { item_id:"2", vendor_id:"2", store_id:"2", price:"454", mrp:"787", instock:"5", discount:"21", status:"active" },
  { item_id:"3", vendor_id:"3", store_id:"3", price:"787", mrp:"876", instock:"5", discount:"45", status:"active" },
  { item_id:"4", vendor_id:"4", store_id:"4", price:"656", mrp:"987", instock:"5", discount:"34", status:"active" },
  { item_id:"5", vendor_id:"5", store_id:"5", price:"345", mrp:"555", instock:"5", discount:"22", status:"active" },
  { item_id:"6", vendor_id:"1", store_id:"1", price:"343", mrp:"766", instock:"5", discount:"34", status:"active" },
  { item_id:"7", vendor_id:"2", store_id:"2", price:"575", mrp:"676", instock:"5", discount:"32", status:"active" },
  { item_id:"8", vendor_id:"3", store_id:"3", price:"898", mrp:"988", instock:"5", discount:"12", status:"active" },
  { item_id:"9", vendor_id:"4", store_id:"4", price:"321", mrp:"766", instock:"5", discount:"16", status:"active" },
  { item_id:"10", vendor_id:"5", store_id:"5", price:"775", mrp:"878", instock:"5", discount:"15", status:"active" },
  { item_id:"11", vendor_id:"1", store_id:"1", price:"245", mrp:"545", instock:"5", discount:"11", status:"active" },
  { item_id:"12", vendor_id:"2", store_id:"2", price:"644", mrp:"766", instock:"5", discount:"10", status:"active" }
  ])
print "Created ItemInventory "
puts "  ================ "