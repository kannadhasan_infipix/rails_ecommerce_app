class Api::Wishlist::V1::WishlistController < Api::Wishlist::V1::ApiController 
   skip_before_filter :verify_authenticity_token
  before_action :display_menus, :user_account_detail
  def new
    @user_wishlist = ::UserWishlist.new
  end

  def index
    @user_wishlist = ::Item.select("items.id, items.image, items.name,items.heading, item_inventories.price, item_inventories.discount").joins(:item_inventory).joins(:UserWishlist).where("user_wishlists.user_id=?",@current_user.id)
#render json: {status: @user_wishlist}
  end

  def show
    begin
      @user_wishlist = ::UserWishlist.find(params[:id])
    rescue ActiveRecord::RecordNotFound => e
      @user_wishlist = []
    end
  end

  def edit
    begin
      @user_wishlist = ::UserWishlist.find(params[:id])
    rescue ActiveRecord::RecordNotFound => e
      @user_wishlist = []
    end
  end

  def create
    begin

     user_wishlist_exist = ::UserWishlist.find_by("user_id=? and item_id=?", @current_user.id, params[:user_wishlist][:item_id]) rescue nil
      render json: {status: false, errors: ['User Wishlist already exist.']} and return if !user_wishlist_exist.blank?
      @user_wishlist = ::UserWishlist.new(wishlist_params_with_user)
      if @user_wishlist.save!
        render json: {status: true, data: {user_wishlist: @user_wishlist}}
      else
        errors = @user_wishlist.errors.messages.each.map {|k,v| v}
        render json: {status: false, errors: errors.flatten }
      end
    rescue Exception => e  
      render json: {status: :false, error: "User Wishlist creation failed" ,  error_message:e.message} 
    end
  end

  def update
    begin
      @user_wishlist = ::UserWishlist.where("user_id=? and item_id=?", @current_user.id, params[:user_wishlist][:item_id]) 
      if @user_wishlist.update(create_params)
        render json: {status: true, data: {user_wishlist: @user_wishlist}}
      else
        errors = @user_wishlist.errors.messages.each.map {|k,v| v}
        render json: {status: false, errors: errors.flatten }
      end
    rescue ActiveRecord::RecordNotFound => e
      render json: {status: :false, error: "User Wishlist not found",  error_message:e.message} 
    end
  end

  def delete
    begin
      @user_wishlist = ::UserWishlist.where("user_id=? and item_id=?", @current_user.id, params[:item_id]) 
    
      if @user_wishlist.destroy_all
        render json: {status: true, message: 'Item remved from Wishlist.'}
      else
        render json: {status: :false, error: "User Wishlist details deleted failed."}
      end 
    rescue ActiveRecord::RecordNotFound => e
      render json: {status: false, error: "User Wishlist not found",  error_message:e.message} 
    end
  end

  def update_status
    begin
      @user_wishlist = ::UserWishlist.find(params[:id])
      @user_wishlist.status = params[:status]
      if @user_wishlist.save!
        flash[:notice] = "User Wishlist Status successfully Updated"
      else
        errors = @user_wishlist.errors.messages.each.map {|k,v| v}
        render json: {status: false, errors: errors.flatten }
      end
    rescue ActiveRecord::RecordNotFound => e
      render json: {status: :false, error: "User Wishlist not found",  error_message:e.message} 
    end
  end

  private


  def wishlist_params_with_user
    create_params.merge({user_id: @current_user.id})
  end

  def create_params
    params.require(:user_wishlist).permit(:item_id, :user_id, :status)
  end
end
