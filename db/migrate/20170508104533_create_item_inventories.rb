class CreateItemInventories < ActiveRecord::Migration
  def change
    create_table :item_inventories do |t|
      t.references :item, index: true, foreign_key: true
      t.references :vendor, index: true, foreign_key: true
      t.references :store, index: true, foreign_key: true
      t.integer :price
      t.integer :mrp
      t.integer :instock
      t.integer :discount
      t.integer :status

      t.timestamps null: false
    end
  end
end
