class Api::UserAccount::V1::UserAccountController < Api::UserAccount::V1::ApiController
 skip_before_filter :verify_authenticity_token
  before_action :display_menus, :user_account_detail
  def new
    @user_account = ::UserAccount.new
  end

  def index
    @user_account = ::UserAccount.all
  end

  def show
    begin
      @user_account = ::UserAccount.find(params[:id])
    rescue ActiveRecord::RecordNotFound => e
      @user_account = []
    end
  end

  def edit
    begin
      @user_account = ::UserAccount.find(params[:id])
    rescue ActiveRecord::RecordNotFound => e
      @user_account = []
    end
  end

  def create
    begin
      user_account_exist = ::UserAccount.find_by_user_id([:acc_number]) rescue nil
      render json: {status: false, errors: ['User Account already exist.']} and return if !user_account_exist.blank?
      @user_account = ::UserAccount.new(user_account_with_user_id)
      if @user_account.save!
        redirect_to "/api/user_account/v1/user_account/"
       # render json: {status: true, data: {user_account: @user_account}}
      else
        errors = @user_account.errors.messages.each.map {|k,v| v}
        render json: {status: false, errors: errors.flatten }
      end
    rescue Exception => e  
      render json: {status: :false, error: "User Account creation failed" ,  error_message:e.message} 
    end
  end

  def update
    begin
      @user_account = ::UserAccount.find(params[:id])
      if @user_account.update(create_params)
        render json: {status: true, data: {user_account: @user_account}}
      else
        errors = @user_account.errors.messages.each.map {|k,v| v}
        render json: {status: false, errors: errors.flatten }
      end
    rescue ActiveRecord::RecordNotFound => e
      render json: {status: :false, error: "User Account not found",  error_message:e.message} 
    end
  end

  def delete
    begin
      @user_account = ::UserAccount.find(params[:id])
      if @user_account.destroy
        render json: {status: true, notice: ' User Account is successfully destroyed.'}
      else
        render json: {status: :false, error: "User Account details deleted failed."}
      end 
    rescue ActiveRecord::RecordNotFound => e
      render json: {status: false, error: "User Account not found",  error_message:e.message} 
    end
  end

  def update_status
    begin
      @user_account = ::UserAccount.find(params[:id])
      @user_account.status = params[:status]
      if @user_account.save!
        flash[:notice] = "User Account Status successfully Updated"
      else
        errors = @user_account.errors.messages.each.map {|k,v| v}
        render json: {status: false, errors: errors.flatten }
      end
    rescue ActiveRecord::RecordNotFound => e
      render json: {status: :false, error: " User Account not found",  error_message:e.message} 
    end
  end
  
  private

  def user_account_with_user_id
    create_params.merge({user_id: @current_user.id})
  end
  

  def create_params
    params.permit( :acc_number, :acc_name, :bank_name, :bank_branch_name, :branch_address, :ifsc_code, :acc_type, :status )
  end
end
