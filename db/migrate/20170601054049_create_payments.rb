class CreatePayments < ActiveRecord::Migration
  def change
    create_table :payments do |t|
      t.string :session_id
      t.string :transaction_id
      t.string :order_id
      t.integer :amount
      t.string :status

      t.timestamps null: false
    end
  end
end
