class Payment < ActiveRecord::Base
  before_create :assign_session_id

  def self.update_status
    begin
      @payment = ::Payment.find_by("session_id=?",cookies[:payment_session_id])
      @payment.status = params[:status]
      @payment.pgi_id = params[:pgi_id]
      if @payment.save!
        render json: {status: true, data:@payment}
        
      else
        errors = @payment.errors.messages.each.map {|k,v| v}
        render json: {status: false, errors: errors.flatten }
      end
    rescue ActiveRecord::RecordNotFound => e
      render json: {status: :false, error: "Payment not found",  error_message:e.message} 
    end
  end

  private
  def assign_session_id
    self.session_id = genreate_session_id
  end
  def genreate_session_id
    loop do
      new_session_id = SecureRandom.hex(7).upcase
      break new_session_id unless Payment.exists?(session_id: new_session_id)
    end
  end
end
