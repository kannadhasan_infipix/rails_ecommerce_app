class Api::Vendor::V1::ApiController < ApplicationController
  respond_to :json
  protect_from_forgery with: :null_session, if: Proc.new { |c| c.request.format =~ %r{application/json} }
  rescue_from ActiveRecord::RecordNotFound, with: :not_found

  private
 
  def authenticate
    #authenticate_or_request_with_http_token do |token, options|
    @current_user = ::Vendor.find_by_auth_token(cookies[:vendor_auth_token])
   
    unless @current_user
       render html: "<script>alert('Please Login!'); window.location='/';</script>".html_safe
    end
  end
end
