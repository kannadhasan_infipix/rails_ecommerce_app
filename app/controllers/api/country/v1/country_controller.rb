class Api::Country::V1::CountryController < Api::Country::V1::ApiController
  skip_before_filter :verify_authenticity_token
  before_action :display_menus, :user_account_detail
  def new
    @country = ::Country.new
  end

  def index
    @country = ::Country.all
  end

  def show
    begin
      @country = ::Country.find(params[:id])
    rescue ActiveRecord::RecordNotFound => e
      @country = []
    end
  end

  def edit
    begin
      @country = ::Country.find(params[:id])
    rescue ActiveRecord::RecordNotFound => e
      @country = []
    end
  end

  def create
    begin
      country_exist = ::Country.find_by_name(params[:country][:name]) rescue nil
      render json: {status: false, errors: ['Country already exist.']} and return if !country_exist.blank?
      @country = ::Country.new(create_params)
      if @country.save!
        render json: {status: true, data: {country: @country}}
      else
        errors = @country.errors.messages.each.map {|k,v| v}
        render json: {status: false, errors: errors.flatten }
      end
    rescue Exception => e  
      render json: {status: :false, error: "Country creation failed" ,  error_message:e.message} 
    end
  end

  def update
    begin
      @country = ::Country.find(params[:id])
      if @country.update(create_params)
        render json: {status: true, data: {country: @country}}
      else
        errors = @country.errors.messages.each.map {|k,v| v}
        render json: {status: false, errors: errors.flatten }
      end
    rescue ActiveRecord::RecordNotFound => e
      render json: {status: :false, error: "Country not found",  error_message:e.message} 
    end
  end

  def delete
    begin
      @country = ::Country.find(params[:id])
      if @country.destroy
        render json: {status: true, notice: 'Country is successfully destroyed.'}
      else
        render json: {status: :false, error: "Country details deleted failed."}
      end 
    rescue ActiveRecord::RecordNotFound => e
      render json: {status: false, error: "Country not found",  error_message:e.message} 
    end
  end
 
  private

  def create_params
    params.require(:country).permit(:name)
  end
end
