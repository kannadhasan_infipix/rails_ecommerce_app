class Api::ShoppingCart::V1::ShoppingCartController < Api::ShoppingCart::V1::ApiController
  skip_before_filter :verify_authenticity_token

  before_action :display_menus, :user_account_detail
  before_action :authenticate
  def new
    @shopping_cart = ::ShoppingCart.new
  end

  def index
    @shopping_cart = ::Item.select("items.id, items.name,items.heading,items.image, item_inventories.price, item_inventories.discount").joins(:item_inventory).joins(:ShoppingCart).where("shopping_carts.user_id=?",@current_user.id)

  end

  def show
    begin
      @shopping_cart = ::ShoppingCart.find(params[:id])
    rescue ActiveRecord::RecordNotFound => e
      @shopping_cart = []
    end
  end

  def edit
    begin
      @shopping_cart = ::ShoppingCart.find(params[:id])
    rescue ActiveRecord::RecordNotFound => e
      @shopping_cart = []
    end
  end

  def create

    begin
      shopping_cart_exist = ::ShoppingCart.find_by("user_id=? and item_id=?", @current_user.id, params[:shopping_cart][:item_id]) rescue nil
      render json: {status: false, errors: ['Shopping Cart already exist.']} and return if !shopping_cart_exist.blank?
      @shopping_cart = ::ShoppingCart.new(shoppingCart_params_with_user)
      if @shopping_cart.save!
        login_user_account(@current_user.id)
        render json: {status: true, data:{cart_count: @cart,cart_amount: @cart_price}}

      else
        errors = @shopping_cart.errors.messages.each.map {|k,v| v}
        render json: {status: false, errors: errors.flatten }
      end
    rescue Exception => e  
      render json: {status: :false, error: "Shopping Cart creation failed" ,  error_message:e.message} 
    end

  end

  def update
    begin
      @shopping_cart = ::ShoppingCart.find(params[:id])
      if @shopping_cart.update(shoppingCart_params_with_user)
        render json: {status: true, data: {shopping_cart: @shopping_cart}}
      else
        errors = @shopping_cart.errors.messages.each.map {|k,v| v}
        render json: {status: false, errors: errors.flatten }
      end
    rescue ActiveRecord::RecordNotFound => e
      render json: {status: :false, error: "Shopping Cart not found",  error_message:e.message} 
    end
  end
  def user_shopping_cart
    cart_item_count = ::ShoppingCart.select("count(*)").find_by_user_id(@current_user.id)
  end

  def delete
    begin
      @shopping_cart = ::ShoppingCart.where("user_id=? and item_id=?", @current_user.id, params[:item_id]) 
       if @shopping_cart.destroy_all
        render json: {status: true, message: 'Cart Item deleted.'}
      else
        render json: {status: :false, error: "Shopping Cart details deleted failed."}
      end 
    rescue ActiveRecord::RecordNotFound => e
      render json: {status: false, error: "Shopping Cart not found",  error_message:e.message} 
    end
  end

  def update_status
    begin
      @shopping_cart = ::ShoppingCart.find(params[:id])
      @shopping_cart.status = params[:status]
      if @shopping_cart.save!
        flash[:notice] = "Shopping Cart Status successfully Updated"
      else
        errors = @shopping_cart.errors.messages.each.map {|k,v| v}
        render json: {status: false, errors: errors.flatten }
      end
    rescue ActiveRecord::RecordNotFound => e
      render json: {status: :false, error: "Shopping Cart not found",  error_message:e.message} 
    end
  end

  private

  def login_user_account(user_id)

    @cart= 0
    @cart_price= 0
    if !user_id_by_auth_token.blank?
      get_count = ::ShoppingCart.select("item_id").where("user_id=?",user_id)
      get_amount = ::ShoppingCart.select("sum(price) as amount").joins(item: :item_inventory).find_by_user_id(user_id)
      @cart = get_count.count 
      if !get_amount.amount.blank?
        @cart_price= get_amount.amount
      end
    end

  end

  def shoppingCart_params_with_user
    create_params.merge({user_id: @current_user.id})
  end

  def create_params
    params.require(:shopping_cart).permit(:item_id, :status)
  end
end
