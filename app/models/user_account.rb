class UserAccount < ActiveRecord::Base
  belongs_to :user
  enum status: {draft: 0, active: 1, deactive: 2}
end
